import React from 'react';
import Sidebar from "react-sidebar";
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { HashRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { RootState } from './reducers/index';
import * as actions from './actions/';
import Cockpit from './containers/Cockpit';
import Invoices from './containers/Invoices';
import Insurances from './containers/Insurances';
import Login from './containers/Login';
import './App.scss';
import { ReactComponent as IconHamburger } from './assets/icons/hamburger.svg';
import { ReactComponent as IconClose } from './assets/icons/close.svg';

export interface IconProps {
  color: string;
}

const IconHelp: React.FC<IconProps> = ( {color} ) => {
  return (
    <svg viewBox="0 0 26 26">
      <g
        fill="none"
        fillRule="evenodd"
        stroke="none"
        strokeWidth="1"
        transform="translate(-27 -971) translate(28 972)"
      >
        <circle
          cx="12"
          cy="12"
          r="12"
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
        />
        <path
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M9.09 9a3 3 0 015.83 1c0 2-3 3-3 3"
        />
        <rect width="2" height="2" x="11" y="16" fill={color} rx="1" />
      </g>
    </svg>
  );
}


const IconCockpit: React.FC<IconProps> = ( {color} ) => {
  return (
    <svg viewBox="0 0 24 24" className="iconCockpit">
      <defs>
        <linearGradient id="a" x1="47.141%" x2="100%" y1="20.128%" y2="100%">
          <stop offset="0%" stopOpacity="0" />
          <stop offset="100%" stopOpacity="0.5" />
        </linearGradient>
      </defs>
      <g
        fill="none"
        fillRule="evenodd"
        stroke="none"
        strokeWidth="1"
        transform="translate(-28 -438) translate(16 426) translate(12 12)"
      >
        <circle cx="12" cy="12" r="12" fill="url(#a)" style={{display: 'none'}} className="show"/>
        <circle cx="12" cy="12" r="11" stroke={color} strokeWidth="2" className="toggle" />
        <path
          className="toggle"
          fill={color}
          d="M14.051 15.316l-6.154 2.052a1 1 0 01-1.265-1.265l2.052-6.154a2 2 0 011.265-1.265l6.154-2.052a1 1 0 011.265 1.265l-2.052 6.154a2 2 0 01-1.265 1.265z"
        />
        <circle cx="12" cy="12" r="1" fill="#FFF" />
      </g>
    </svg>
  );
}

const IconVersicherte: React.FC<IconProps> = ( {color} ) => {
  return (
    <svg viewBox="0 0 26 20" className="iconVersicherte">
          <defs>
            <linearGradient id="a" x1="47.141%" x2="100%" y1="20.128%" y2="100%">
              <stop offset="0%" stopOpacity="0" />
              <stop offset="100%" stopOpacity="0.5" />
            </linearGradient>
          </defs>
          <g
            fill="none"
            fillRule="evenodd"
            stroke="none"
            strokeWidth="1"
            transform="translate(-28 -504) translate(29 504)"
          >
            <path
              stroke={color}
              className="toggle"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M15 19c0-3-2.813-6-7.5-6C2.812 13 0 16 0 19M17 14.833c.518-.555 1.3-.833 2.349-.833C22.498 14 24 16.5 24 19"
            />
            <circle cx="7.5" cy="5.5" r="5.5" fill="url(#a)" className="show" style={{display: 'none'}}/>
            <circle cx="19" cy="8" r="4" fill="url(#a)" className="show" style={{display: 'none'}}/>
            <circle cx="7.5" cy="5.5" r="4.5" stroke={color} strokeWidth="2" className="toggle"/>
            <circle cx="19" cy="8" r="3" stroke={color} strokeWidth="2" className="toggle" />
          </g>
    </svg>
  );
}

const IconRechnung: React.FC<IconProps> = ( {color} ) => {
  return (
    <svg viewBox="0 0 26 18" className="iconRechnung">
      <g
        fill="none"
        fillRule="evenodd"
        stroke={color}
        className="toggle"
        strokeLinecap="round"
        strokeWidth="2"
        transform="translate(-27 -565) translate(28 566)"
      >
        <rect
          width="24"
          height="16"
          x="0"
          y="0"
          strokeLinejoin="round"
          rx="2"
        />
        <path d="M5 11h6" />
        <path strokeLinejoin="round" d="M0 5h24" />
      </g>
    </svg>
  );
}

export interface AppProps {
  sidebarOpened: boolean;
  authentication: any;
  setSidebar: any;
  // logOut: any;
}

const setBodyClassNames = (opened) => {
  if (opened) {
    document.body.classList.add('opened');
    document.body.classList.remove('closed');
  } else {
    document.body.classList.remove('opened');
    document.body.classList.add('closed');
  }
}

const App: React.FC<AppProps> = ( {sidebarOpened, setSidebar, authentication} ) => {
  console.log("functional compoennt", sidebarOpened, authentication);
  setBodyClassNames(sidebarOpened);

  return (
      <Router>
        {authentication.token && <Sidebar
          rootClassName="sidebarContainer"
          sidebarClassName="sidebar"
          contentClassName="sidebarContent"
          overlayClassName="sidebarOverlay"
          open={sidebarOpened}
          docked={false}
          styles={
            {
              sidebar: {
              }
            }
          }
          onSetOpen={(opened) => {setBodyClassNames(opened); setSidebar(opened)}}
          sidebar={<div></div>}
        >
          <div className="hamburgerContainer d-flex">
            <button onClick={() => {setBodyClassNames(!sidebarOpened); setSidebar(!sidebarOpened)} } style={{zIndex: 2, position: 'absolute', height: '40px',top: '30px', width: '48px'}} className="hamburger">
              {sidebarOpened ? <IconClose /> : <IconHamburger />}
            </button>
            <div className="links">
              <Link className="sidebarLink cockpit" to="/cockpit" onClick={() => { setBodyClassNames(false); setSidebar(false) } }>
                <div className="iconWrapper large">
                  <IconCockpit color="#AFAFB1"/>
                </div>
                <span className="ml-4">
                  Cockpit
                </span>
              </Link><br/>
              <Link className="sidebarLink versicherte" to="/versicherte/personen" onClick={() => { setBodyClassNames(false); setSidebar(false) } }>
                <div className="iconWrapper large">
                  <IconVersicherte color="#AFAFB1"/>
                </div>
                <span className="ml-4">
                  Versicherte
                </span>
              </Link><br/>
              <Link className="sidebarLink rechnung" to="/rechnungen" onClick={() => { setBodyClassNames(false); setSidebar(false) } }>
                <div className="iconWrapper large">
                  <IconRechnung color="#AFAFB1"/>
                </div>
                <span className="ml-4">
                Rechnungen
                </span>
              </Link><br/>

            </div>

            <Link className="sidebarLink support" to="/rechnungen" onClick={() => { setBodyClassNames(false); setSidebar(false) } }>
              <div className="iconWrapper large">
                <IconHelp color="#AFAFB1"/>
              </div>
              <span className="ml-4 d-none d-sm-inline">
              Support
              </span>
            </Link><br/>
{/*            <Link className="help mb-4 text-left  d-none d-sm-flex" to="/">
              <div className="iconWrapper large" onClick={logOut}>
                LOGOUT
              </div>
            </Link>*/}

          </div>
        </Sidebar>}
        <Route exact path="/" component={Login} />
        <Route path="/cockpit"
          render={props =>
            authentication.token !== 3648196 ? (
              <Redirect to="/" />
            ) : (
              <Cockpit
                {...props}
              />
            )}
        />

        <Route path="/versicherte/:tab?"
          render={props =>
            authentication.token !== 3648196 ? (
              <Redirect to="/" />
            ) : (
              <Insurances
                {...props}
              />
            )}
        />
        <Route path="/rechnungen"
          render={props =>
            authentication.token !== 3648196 ? (
              <Redirect to="/" />
            ) : (
              <Invoices
                {...props}
              />
            )}
        />
      </Router>
  );
}


function mapStateToProps(state: RootState, ownProps: any) {
  return {
    ...ownProps,
    ...state.app,
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ACTION>) {
  return {
    setSidebar: (val) => dispatch(actions.setSidebar(val)),
    logOut: () => dispatch(actions.logOut()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

// export default App;
