import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/index.scss';
import 'react-redux-toastr/src/styles/index.scss'
import * as serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware, Store } from 'redux';
import { Route} from "react-router-dom";
import { ConnectedRouter, routerMiddleware } from 'connected-react-router'
import createRootReducer, { RootState } from './reducers/index';
import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr'

import { createHashHistory } from 'history'
console.log("index", process.env);
const historyS = createHashHistory();

let middleware = applyMiddleware(
  routerMiddleware(historyS), // for dispatching history actions
  thunk,
  logger,
);

const store = createStore(
    createRootReducer(historyS) as any,
    {} as any,
    middleware as any
  ) as Store<RootState>;

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={historyS}>
      <Route path="/" component={App} />
      <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="top-left"
        getState={(state) => state.toastr} // This is the default
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar
        closeOnToastrClick
      />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root') as HTMLElement
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
