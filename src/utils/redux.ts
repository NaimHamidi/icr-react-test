// Get Requests

export enum EGetRequestStatus {
  None = 'None',
  Fetching = 'Fetching',
  Fetched = 'Fetched',
  Error = 'Error'
}

export interface IGetRequest<RES> {
  status: EGetRequestStatus;
  responseData?: RES;
  error?: any;
}

export function getRequestInit(initValue) {
  return {
    status: EGetRequestStatus.None,
    value: initValue
  };
}

// Post Requests

export enum EPostRequestStatus {
  None = 'None',
  Fetching = 'Fetching',
  Fetched = 'Fetched',
  Error = 'Error'
}

export interface IPostRequest<REQ, RES> {
  status: EGetRequestStatus;
  requestData?: REQ;
  responseData?: RES;
  error?: any;
}

export function postRequestInit(initData) {
  return {
    status: EGetRequestStatus.None,
    requestData: initData
  };
}
