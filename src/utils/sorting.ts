export default function Comparator(a, b, direction, string) {
  if (string) {
    return a.localeCompare(b) * direction;
  }
  return (a - b) * direction;
}
