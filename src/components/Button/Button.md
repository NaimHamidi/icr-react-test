Pill Small
```js
<Button buttonType={'Pill Small'}>
  Pill Small
</Button>
```

Inverted Pill Medium
```js
<Button buttonType={'Inverted Pill Medium'}>
  Inverted Pill Medium
</Button>
```


Pill Medium
```js
<Button buttonType={'Pill Medium'}>
  Pill Medium
</Button>
```

Full Width
```js
<Button buttonType={'Full Width'}>
  Full Width
</Button>
```

Basic Link
```js
<Button buttonType={'Basic Link'}>
  Basic Link
</Button>
```
