import React from 'react';
import { connect } from 'react-redux';
import { Link} from "react-router-dom";
import { Dispatch } from 'redux';
import { RootState } from '../../../reducers/index';
import * as actions from '../../../actions/';
import { ReactComponent as IconPdfDownload } from '../../../assets/icons/pdfdownload.svg';
import { ReactComponent as IconEdit } from '../../../assets/icons/edit.svg';
import './Mutation.scss';

export interface MutationProps {
  changeable: boolean;
  type: string;
  owner: string;
  no: string;
}

const Mutation: React.FC<MutationProps> = ( {changeable, type, owner, no} ) => {
  return (
      <div className="mutationContainer container-fluid">
        <div className="row">
          <div className="col-lg-6 col-12 mutationType">
            {type}
          </div>
          <div className="col-lg-6 col-12 bearbeiten">
            {changeable ?
              <div className="iconWrapper" style={{width: '28px', textAlign: 'center', verticalAlign: 'top'}}>
                <Link to={`/versicherte/personen?edit=${no}&content=Lohn+anpassen`}>
                  <IconEdit />
                </Link>
              </div>
               : null}
            <div className="iconWrapper" style={{width: '28px', marginLeft: '8px', textAlign: 'center', height: '25px'}}>
              <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer">
                <IconPdfDownload />
              </a>
            </div>
          </div>
        </div>

        <div className="row p-relative" style={{marginTop: "18px"}}>
          <div className="col-12 d-flex justify-content-between">
            <div className="font-16">
              {owner}
            </div>
            <div className="font-16 ml-4">
              {no}
            </div>
          </div>
        </div>
      </div>
  );
}


function mapStateToProps(state: RootState, ownProps: any) {
  return {
    ...ownProps,
    ...state.app,
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ACTION>) {
  return {
    // setSidebar: (val) => dispatch(actions.setSidebar(val)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mutation);
