import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { RootState } from '../../../reducers/index';
import * as actions from '../../../actions/';
import classNames from 'classnames';
import { ReactComponent as IconPdfDownload } from '../../../assets/icons/pdfdownload.svg';
import { ReactComponent as IconCheckmarkWhite } from '../../../assets/icons/checkmark-bold-white.svg';
import './Invoice.scss';

export interface InvoiceProps {
  state: string;
  amount: string;
  number: string;
  date: string;
}

const Invoice: React.FC<InvoiceProps> = ( {number, state, amount, date} ) => {
  return (
      <div className="mutationContainer container-fluid">
        <div className="row">
          <div className="col-12 d-flex">
            <div className="mutationType">
              {number}
            </div>
            <div className="ml-4 state">
              {state === 'offen' ?
              <div className={classNames('pill', {
                      open: state === 'offen'
                    })}
              >
              {state}
              </div>
              :
              <div className={classNames('pill', {
                  closed: state === 'ok',
                  delayed: state === 'überfällig'
                })}
              >
              {state === 'ok' ? <IconCheckmarkWhite /> : state}
              </div>
            }
            </div>

            <div className="iconWrapper" style={{width: '28px', marginLeft: 'auto', textAlign: 'center', height: '25px'}}>
              <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer">
                <IconPdfDownload />
              </a>
            </div>
          </div>
        </div>

        <div className="row p-relative" style={{marginTop: "11px"}}>
          <div className="col-12 d-flex">
            <div className="font-16">
              {amount}
            </div>
            <div className="font-16 ml-4">
              {date}
            </div>

{/*            <div className="iconWrapper" style={{position: 'absolute', right: '15px', bottom: '0'}}>
              <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer">
                <IconDownload />
              </a>
            </div>*/}
          </div>
        </div>
      </div>
  );
}


function mapStateToProps(state: RootState, ownProps: any) {
  return {
    ...ownProps,
    ...state.app,
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ACTION>) {
  return {
    // setSidebar: (val) => dispatch(actions.setSidebar(val)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Invoice);
