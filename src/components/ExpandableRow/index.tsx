import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../reducers/index';
import { Dispatch } from 'redux';
import classNames from 'classnames';
import * as actions from '../../actions/';
import './ExpandableRow.scss';
import { ReactComponent as IconArrowDown } from '../../assets/icons/arrow-down.svg';


interface State {
    expanded: boolean;
}

interface Props {
  col1: string;
  col2: string;
  col3: string;
  col4: any;
  col5: string;
  col6: any;
  col7: any;
  moreContent: any;
  rowChanged?: any;
  className?: string;
}

class ExpandableRow extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      expanded: false
    };

    console.log("ExpandableRow constructor called");
  }

  componentDidMount() {
    console.log('ExpandableRow app did mount', this.props);
  }

  componentWillUnmount() {
  }

  expandRow = () => {
    this.setState({ expanded: true });
    this.props.rowChanged && this.props.rowChanged(true);
  }

  collapseRow = () => {
    this.setState({ expanded: false });
    this.props.rowChanged && this.props.rowChanged(false);
  }

  render() {
    return (
        <div className={classNames('exRowTop', this.props.className, {
          expanded: this.state.expanded
            }
          )}>
          <div className={classNames('exRow', 'icr-row')}>

            <div className={classNames('column', 'col1', 'invoiceNumber')}>
              {this.props.col1}
            </div>

            <div
              className={classNames(
                'column',
                'col2'
              )}
            >
              {this.props.col2}
            </div>

            <div className={classNames('column', 'col3')}>
              {this.props.col3}
            </div>
            <div
              className={classNames('column', 'col4')}
            >
              {this.props.col4}
            </div>
            <div
              className={classNames('column', 'col5')}
            >
              {this.props.col5}
            </div>
            <div className={classNames('column', 'col6 icon')}>
              {this.props.col6}
            </div>
            {this.props.col7 && <div className={classNames('column', 'col7 icon')}>
                {this.props.col7}
              </div>}
            {this.props.moreContent && <div
              className={classNames('column', 'col7 icon')}
            >
              {this.state.expanded ? (
                <button
                  className={'moreDetails anchorLike'}
                  style={{ position: 'relative' }}
                  onClick={this.collapseRow}
                >
                  <div className={'arrow rotated'}>
                    <IconArrowDown />
                  </div>
                </button>
              ) : (
                <button
                  className={'moreDetails anchorLike'}
                  style={{ position: 'relative' }}
                  onClick={this.expandRow}
                >
                  <div className={'arrow'}>
                    <IconArrowDown />
                  </div>
                </button>
              )}
            </div>}
          </div>

          {this.props.moreContent && <div
            className={classNames(
              'exRow',
              'moreRow',
              this.state.expanded ? 'active' : ''
            )}
          >
            {this.props.moreContent}
          </div>}

        </div>
    );
  }
}

function mapStateToProps(state: RootState, ownProps: any) {
  return {
    ...ownProps,
    ...state.app,
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ACTION>) {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpandableRow);
