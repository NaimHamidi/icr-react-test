import * as React from 'react';
import classNames from 'classnames';
import { exists } from '../../../utils/variableEvaluation';
import { EInputTextType } from '../index';
import styles from './InputText.module.scss'; // Import css modules stylesheet as styles

interface IProps {
  isFocused: boolean;
  isLoading: boolean;
  name?: string;
  value?: string[] | string | number;
  placeholder: string;
  type: string;
  onChange?: (name: string, value: string) => void;
  additionalClass?: string;
  isDisabled?: boolean;
  inputTextType?: EInputTextType;
  onBlur?: any;
  autoFocus?: boolean;
}

export class InputText extends React.Component<IProps, {}> {
  private inputRef: HTMLInputElement;
  private focusTimeoutId;

  public componentDidMount() {
    this.focusTimeoutId = setTimeout(() => {
      if (this.props.isFocused) {
        this.inputRef.focus();
      }
    }, 0);
  }

  public componentWillUnmount() {
    clearTimeout(this.focusTimeoutId);
  }

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log("handleChange",e.currentTarget);
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;
    this.props.onChange && this.props.onChange(name, value);
  };

  public render() {
    const {
      name,
      value,
      placeholder,
      type,
      isFocused,
      isLoading,
      additionalClass,
      isDisabled,
      inputTextType,
      onBlur
    } = this.props;

    const inputClass = classNames(styles.inputText, {
      [additionalClass]: exists(additionalClass),
      [styles.hasValue]: exists(value),
      [styles.textStandard]: inputTextType === EInputTextType.Standard,
      [styles.textMaterial]: inputTextType === EInputTextType.Material
    });

    return (
      <input
        name={name}
        type={type}
        placeholder={placeholder}
        value={value}
        onBlur={onBlur}
        onChange={this.handleChange}
        className={inputClass}
        ref={input => isFocused && (this.inputRef = input)}
        autoFocus={isFocused}
        disabled={isDisabled || isLoading}
      />
    );
  }
}
