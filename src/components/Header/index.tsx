import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../reducers/index';
import { Dispatch } from 'redux';
import * as actions from '../../actions/';
import './Header.scss';
import { DropdownCollapse } from '../DropdownCollapse/index';
import { Button, EButtonType } from '../../components/Button/index';
import { ReactComponent as IconLogout } from '../../assets/icons/logout.svg';


interface State {
}

interface Props {
  title: string;
  avatarSrc: any;
  logOut: any;
}

class Header extends React.Component<Props, State> {
  constructor(props: any) {
    console.log("Header constructor called");

    super(props);
  }

  componentDidMount() {
    console.log('Header app did mount', this.props);
  }

  componentWillUnmount() {
  }


  render() {
    return (
      <header className="headerContainer">
        <div className="headerTitle">{this.props.title}</div>
        <div className="headerContent">
            {this.props.children}
        </div>
{/*        <div className="headerAvatar"><img alt="avatar" src={this.props.avatarSrc}/></div>*/}
        <DropdownCollapse label="Thomas Meier">
            <h2 >Thomas Meier</h2>
            <p className="email">thomas.meier@musterfirma.ch</p>
            {/*<Button
              className="d-inline-block w-auto mt-4 ml-auto text-uppercase"
              onClick={() => { console.log("password forgot clicked") }}
              to={null}
              buttonType={EButtonType.BasicLink}
            >
              Passwort ändern
            </Button>*/}
            <Button
              className="d-flex text-uppercase font-14 text-center mt-40 invertedRed justify-content-center"
              onClick={() => { this.props.logOut()}}
              to={null}
              buttonType={EButtonType.FullWidthInverted}
            >
              Abmelden
              <div className="iconWrapper small ml-4" style={{height: '20px', marginTop: '-5px'}}>
                <IconLogout className=""/>
              </div>
            </Button>

        </DropdownCollapse>
      </header>
    );
  }
}

function mapStateToProps(state: RootState, ownProps: any) {
  return {
    ...ownProps,
    ...state.app,
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ACTION>) {
  return {
      logOut: () => dispatch(actions.logOut()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
