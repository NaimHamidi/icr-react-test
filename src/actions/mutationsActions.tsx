import * as constants from '../constants';
import axios from 'axios';

export const getMutations = () => async(dispatch) =>{
    try{
        const response = await axios.get('https://60e5cd64086f730017a6fe19.mockapi.io/api/icr-react-rest/mutations');

        dispatch({type: constants.GET_MUTATIONS, payload: response})

        return response
    }catch(error){
        throw error
    }
}