import * as constants from '../constants';
import { push } from 'connected-react-router'


export interface ACTION {
    type: string;
    payload?: any;
}

export const setSidebar = (payload: boolean): ACTION => ({
  type: constants.SIDEBAR_SET,
  payload
});

export const loginAct = (payload: any): ACTION => ({
  type: constants.LOGIN,
  payload
});


export const logOut = (): ACTION => ({
  type: constants.LOG_OUT,
});

export function login(payload) {
    console.log("login action", payload);
    return dispatch => {

      const loginPromise = new Promise((resolve, reject) => {
        dispatch(loginAct(payload));
          if (payload.token.hashCode() === 3648196) {
            console.log("will push cockpit");
            // setTimeout(() => {
              dispatch(push('/cockpit'));
            // }, 150)
            resolve("Logged in successfully");
          }else{
            reject("The user credentials were incorrect");
          }
      });

      const response = loginPromise;
      return response;
    };
}
