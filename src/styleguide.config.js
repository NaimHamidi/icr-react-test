module.exports = {
   sections: [

    {
      name: 'Components Documentation',
      content: 'src/docs/login.md',
      components: 'src/components/Login/*.js',
      exampleMode: 'expand', // 'hide' | 'collapse' | 'expand'
      usageMode: 'expand' // 'hide' | 'collapse' | 'expand'
    }
  ]
}
