import { combineReducers } from 'redux';
import app, { AppState } from './app';
import { connectRouter } from 'connected-react-router'
import {reducer as toastrReducer} from 'react-redux-toastr'


export interface RootState {
  app: AppState;
}

export default (history) => combineReducers({
  router: connectRouter(history),
  app,
  toastr: toastrReducer
})
