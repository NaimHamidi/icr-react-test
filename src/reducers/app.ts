import { SIDEBAR_SET, LOGIN, LOG_OUT, GET_MUTATIONS } from '../constants/index';

// const mutations = require('../data/GetMutations.json');
const insurant = require('../data/CreateInsurant.json');
const all = require('../data/GetAll.json');

export interface AppState {
    sidebarOpened: boolean;
    avatarSrc: string;
    mutations: any;
    insurant: any;
    all: any;
    invoices: any;
}

const getInitialState = () => {
    return {
        // authentication: process.env.NODE_ENV === "development" ? {
        //     login: 'dev',
        //     token: 3648196
        // } : { login: null, token: null },
        authentication: { login: null, token: null },
        sidebarOpened: false,
        avatarSrc: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/unknown-512.png",
        mutations: [],
        insurant: insurant.insurantDetails,
        all: all,
        invoices: [
            {
                number: "CH99887",
                state: "überfällig",
                amount: 4632.11,
                date: "01.01.2019",
                m_sEmployerName: "Firma 144"
            },
            {
                number: "GL898",
                state: "offen",
                amount: 2100.115,
                date: "01.013.2019",
                m_sEmployerName: "Firma 145"
            },
            {
                number: "CH9873",
                state: "offen",
                amount: 2000.11,
                date: "01.05.2019",
                m_sEmployerName: "Firma 144"
            },
            {
                number: "GL898",
                state: "ok",
                amount: 1000.11,
                date: "31.12.2018",
                m_sEmployerName: "Firma 145"
            },
        ]
        // events: {
        //   1: {
        //     name: 'eintritt'
        //   },
        //   2: {
        //     name: 'adressanderung'
        //   },
        //   3: {
        //     name: 'pensionierung'
        //   }
        // },
        // invoiceStatuses: ['open', 'closed', 'done', 'delayed'],
        // mutations: null,
        // invoices: null,
        // people: null
    }
};

console.log("app.ts", getInitialState());

const app = (state: AppState = getInitialState(), action: any) => {
    switch (action.type) {
        case SIDEBAR_SET: {
            console.log("reducer sidebarset", action);
            return { ...state, sidebarOpened: action.payload };
        }
        case LOGIN: {
            console.log("reducer login", action);
            if (action.payload.token.hashCode() === 3648196) {
                console.log("true");
                return {
                    ...state, authentication: {
                        login: action.payload.login,
                        token: 3648196
                    }
                };
            } else {
                console.log("else");
                return { ...state }
            }
        }
        case LOG_OUT: {
            console.log("reducer logout", action);
            return { ...state, authentication: getInitialState().authentication };
        }
        case GET_MUTATIONS: {
            return { ...state, mutations: action.payload.data };
        }
    }
    return state;
};

export default app;
