import React from 'react';
import {connect} from 'react-redux';
import {RootState} from '../../reducers/index';
import {Dispatch} from 'redux';
import Modal from 'react-modal';
import Tab from '@material/react-tab';
import * as actions from '../../actions/';
import Header from '../../components/Header';
import ExpandableRow from '../../components/ExpandableRow';
import Drawer from '../../components/Drawer';
import {Button, EButtonType} from '../../components/Button/index';
import {Input} from '../../components/Input/index';
import classNames from 'classnames';
import {EValidationTestType} from '../../utils/validationConfigs';
import {renderSwitch} from '../../utils/react';
import {getParameterByName} from '../../utils/toSearchParams';
import { getMutations } from '../../actions/mutationsActions';
// import { WithContext as ReactTags } from 'react-tag-input';
// import { EValidationTestType } from '../../utils/validationConfigs';
import {ReactComponent as IconFilter} from '../../assets/icons/filter.svg';
import {ReactComponent as IconPdfDownload} from '../../assets/icons/pdfdownload.svg';
import {ReactComponent as IconEdit} from '../../assets/icons/edit.svg';
import {ReactComponent as IconPen} from '../../assets/icons/pen.svg';
import {ReactComponent as IconPlus} from '../../assets/icons/plus.svg';
import {ReactComponent as IconArrowRight} from '../../assets/icons/arrow-right.svg';
// import {ReactComponent as IconCheckmarkWhite} from '../../assets/icons/checkmark-bold-white.svg';
import {ReactComponent as IconCheckmarkSlim} from '../../assets/icons/checkmark-slim.svg';
import './Insurances.scss';

// const statusOptions = [
//     {
//         id: '1',
//         dropdownLabel: 'Inaktiv'
//     }, {
//         id: '2',
//         dropdownLabel: 'geändert'
//     },
//     {
//         id: '3',
//         dropdownLabel: 'other'
//     }
// ];

const firmaOptions = [
    {
        id: '154',
        dropdownLabel: 'Firma 144'
    }, {
        id: '155',
        dropdownLabel: 'Firma 145'
    }
];

const genderOptionsSelectForFilter = [
    {
        id: '153',
        dropdownLabel: 'Male'
    },
    {
        id: '154',
        dropdownLabel: 'Female'
    },
    {
        id: '155',
        dropdownLabel: 'Other'
    }, {
        id: '156',
        dropdownLabel: 'Male to Female'
    }
]

const countryOptionsSelectForFilter = [
    {
        id: '153',
        dropdownLabel: 'Brazil'
    },
    {
        id: '154',
        dropdownLabel: 'Norway'
    },
    {
        id: '155',
        dropdownLabel: 'Angola'
    }, {
        id: '156',
        dropdownLabel: 'Kazakhstan'
    }
]

const ereignisOptions = [
    {
        id: '1',
        dropdownLabel: 'Lohnaenderung'
    }, {
        id: '2',
        dropdownLabel: 'Other'
    }
];


const salutationOptions = [
    {
        id: '1',
        dropdownLabel: 'Herr'
    }, {
        id: '2',
        dropdownLabel: 'Frau'
    }
]

interface State {
    filtersOpened: boolean;
    currentTab: string;
    onlyChangedFilter: boolean;
    alsoInactiveFilter: boolean;
    alsoClosedMutationsFilter: boolean;
    modalIsOpen: boolean;
    modalContentId: string;
    modalContent: string;
    lohnAnpassenForm: any;
    eintrittErfassenForm: any;
    versicherteInfosForm: any;
    newEntryStep: number;
    personSearchFilter: string;
    personSearchFilterMutations: string;
    firmaFilter: string;
    genderFilterMutations: string;
    countryFilterMutations: string;
    filteredRowsPersons: any;
    filteredRowsMutations: any;
}

interface Props {
    mutations: any;
    invoices: Array<any>;
    all: any;
    location: any;
    history: any;
    match: any;
    getMutations: any;
    // setSidebar: any;
}

Modal.setAppElement('#root');

class Insurances extends React.Component<Props, State> {
    constructor(props : any) {
        super(props);
        document.body.classList.add("insurancesPage");
        console.log("constructor", this.props.location.search, );
        let modalContentId = '';
        let modalContent = '';

        if (this.props.location.search && this.props.location.search.length > 0) {
            // const url = new URL(this.props.location.search);
            // const url = new URL(window.location.href);
            modalContentId = getParameterByName('edit', this.props.location.search);
            modalContent = getParameterByName('content', this.props.location.search);
            // tabId = Number(getParameterByName('tab', this.props.location.search));
            console.log("modalContentId", modalContentId, 'modalContent', modalContent);
        }

        this.state = {
            filtersOpened: window.innerWidth >= 1400,
            currentTab: props.match.params.tab,
            onlyChangedFilter: false,
            alsoInactiveFilter: false,
            modalIsOpen: modalContentId ? true : false,
            modalContentId: modalContentId,
            modalContent: modalContent,
            lohnAnpassenForm: {},
            eintrittErfassenForm: {},
            versicherteInfosForm: {},
            newEntryStep: 1,
            personSearchFilter: "",
            firmaFilter: "",
            filteredRowsPersons: props.all.Data,
            filteredRowsMutations: props.mutations,
            alsoClosedMutationsFilter: false,
            countryFilterMutations: "",
            genderFilterMutations: "",
            personSearchFilterMutations: ""
        }
    }

    private subtitle = null;
    private drawer = null;
    // private languageOptions: any = null;

    viewPortChanged = (e) => {
        console.log("viewPortChanged", e);
        setTimeout(() => {
            if (e.target.innerWidth >= 1400 && (this.state.filtersOpened === false)) {
                this.setState({filtersOpened: true});
            }
        }, 500);
    }


    componentDidMount() {
        console.log('insurances did mount', this.props);
        document.body.classList.remove('backdrop');
        window.addEventListener("resize", this.viewPortChanged);
        this.filterMutations('alsoClosedCheckboxMutations',false);
        this.filterPersons('alsoInactiveCheckbox',false);
        this.props.getMutations().then(res => {
            console.log('Mutations from API')
        });
        // this.props.setSidebar(false);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.viewPortChanged);
        document.body.classList.remove("insurancesPage");
    }

    static getDerivedStateFromProps(props, state) {
        // Update state so the next render will show the fallback UI.
        console.log("getDerivedStateFromProps", props.location.search, state.modalIsOpen, props.match.params.tab);
        if (state.modalIsOpen && props.location.search === '') {
            // Will hide modal as search is empty
            return {
              modalIsOpen: false,
              currentTab: props.match.params.tab
            };
        } else {
            console.log("getDerivedStateFromProps else");
            return {
              currentTab: props.match.params.tab,
              modalContent: getParameterByName('content', props.location.search),
              modalContentId: getParameterByName('edit', props.location.search)
            }
        }
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //   console.log("shouldComponentUpdate");
    //   return true;
    // }

    handleContentContainerClick = (e) => {
        console.log("handleContentContainerClick", e, e.target, e.currentTarget);
        if (this.drawer.contains(e.target)) {
            return;
        } else {
            window.innerWidth < 1400 && this.setState({filtersOpened: false});
        }
    }

    openModal = (id, content) => {
        this.setState({modalIsOpen: true, modalContent: content, modalContentId: id});
        this.props.history.push({
            pathname: `/versicherte/${this.state.currentTab}`,
            search: "?" + new URLSearchParams({edit: id, content}).toString()
        })
    }

    afterOpenModal = () => {
        // references are now sync'd and can be accessed.
        // this.subtitle.style.color = '#f00';
    }

    closeModal = () => {
        this.setState({modalIsOpen: false});
        this.props.history.push({
          pathname: `/versicherte/${this.state.currentTab}`,
          search: ''
       })
    }

    handleRadioChange = (name ,value) => {
        this.setState({lohnAnpassenForm: {...this.state.eintrittErfassenForm, name: value}});
    }


    languageOptions = () => [
      {
          id: '1',
          label: 'DE',
          validationMessageConfig: null,
          validationTests: [EValidationTestType.hasValue],
          type: 'radio',
          name: "languageRadios",
          inputWrapperClass: "w-100 high",
          value: this.state.eintrittErfassenForm.language,
          onChange: this.handleRadioChange
      }, {
          id: '2',
          label: 'FR',
          validationMessageConfig: null,
          validationTests: [EValidationTestType.hasValue],
          type: 'radio',
          name: "languageRadios",
          inputWrapperClass: "w-100 high",
          value: this.state.eintrittErfassenForm.language,
          onChange: (name, value) => {this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, language: value}})}
      }, {
          id: '3',
          label: 'IT',
          validationMessageConfig: null,
          validationTests: [EValidationTestType.hasValue],
          type: 'radio',
          name: "languageRadios",
          inputWrapperClass: "w-100 high",
          value: this.state.eintrittErfassenForm.language,
          onChange: (name, value) => {this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, language: value}})}
      }, {
          id: '4',
          label: 'EN',
          validationMessageConfig: null,
          validationTests: [EValidationTestType.hasValue],
          type: 'radio',
          name: "languageRadios",
          inputWrapperClass: "w-100 high",
          value: this.state.eintrittErfassenForm.language,
          onChange: (name, value) => {this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, language: value}})}
      }
  ];

    parseDate = (
        str, type
        ?) => {
        if (str) {
            var options = {};

            if (type === 'hr-min') {
                options = {
                    // day: 'numeric',
                    // month: '2-digit',
                    // year: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit'
                }
                return new Date(Date.parse(str)).toLocaleTimeString('de-DE', options);
            } else {
                options = {
                    day: 'numeric',
                    month: '2-digit',
                    year: 'numeric',
                    // hour: '2-digit',
                    // minute: '2-digit'
                }
                return new Date(Date.parse(str)).toLocaleDateString('de-DE', options);
            }

        } else
            return ''
    };

    renderState(personState) {
        return <div className="state">
            {
                renderSwitch(personState, {
                    "Inaktiv": () => <div className={classNames('pill', {
                            'inactive': personState === 'Inaktiv'
                        })}>
                        {personState}
                    </div>,
                    "geändert": () => <div className={classNames('pill', {
                            'open': personState === 'geändert',
                        })}>
                        {personState}
                    </div>,
                    "other": () => <div className={classNames('pill empty')}>
                            {/*<IconCheckmarkWhite/>*/}
                        </div>
                })
            }
        </div>
    }

    renderMoreContentPersonen(entry) {
        return (<div className="moreContainer container-fluid flex-wrap">
            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Lohn
                        </div>

                        <div className="value">
                            CHF {entry.m_nGrossSalary}
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column">
                            Bonus
                        </div>

                        <div className="value">
                            CHF {entry.m_nGrossSalary}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column">
                            Kassennummber
                        </div>

                        <div className="value">
                            {entry.m_sSocialSecurityNumber}
                        </div>

                    </div>
                </div>

                <div className="column col4">
                    <div className="flex-column">

                        <div className="title column">
                            Versicherungsnummer
                        </div>

                        <div className="value">
                            {entry.m_sSocialSecurityNumber}
                        </div>

                    </div>
                </div>
            </div>

            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Lohn gültig von
                        </div>

                        <div className="value">
                            {this.parseDate(entry.m_dPayrollValidFrom)}
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column">
                            Arbeitspensum
                        </div>

                        <div className="value">
                            {entry.m_nActivityRate}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column">
                            Arbeitgeber
                        </div>

                        <div className="value">
                            {entry.m_sEmployerName}
                        </div>

                    </div>
                </div>

                <div className="column col4 d-md-inline-flex d-none">
                    <div className="flex-column">

                        <div className="title column"></div>

                        <div className="value"></div>

                    </div>
                </div>
            </div>

            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Lohn gültig bis
                        </div>

                        <div className="value">
                            {this.parseDate(entry.m_dPayrollValidUntil)}
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column">
                            Eintrittsdatum
                        </div>

                        <div className="value">
                            {this.parseDate(entry.m_dBirthDate)}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column">
                            Firma
                        </div>
                        <div className="value">
                            {entry.m_sFirstname}
                        </div>

                    </div>
                </div>

                <div className="column col4  d-md-inline-flex d-none">
                    <div className="flex-column">

                        <div className="title column"></div>
                        <div className="value"></div>

                    </div>
                </div>
            </div>
            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Adresse
                        </div>
                        <div className="value">
                            {entry.m_sFirstname}
                            15
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">
                        <div className="title column ">
                            Zivilstand

                        </div>

                        <div className="value showChildOnHover">
                            {entry.m_sName}
                            <div className="iconWrapper small ml-3 pointer showable" style={{}} onClick={() => {
                               this.openModal(entry.m_sSocialSecurityNumber, "Zivilstand");
                            }}><IconPen/></div>
                        </div>
                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">
                        <div className="title column"></div>
                        <div className="value"></div>
                    </div>
                </div>

                <div className="column col4  d-md-inline-flex d-none">
                    <div className="flex-column">
                        <div className="title column"></div>
                        <div className="value"></div>
                    </div>
                </div>
            </div>

        </div>)
    }

    rowChanged(expanded) {
      console.log("rowChanged in insurances", expanded);
      if (expanded) {

      }
    }


    filterMutations(name, value) {
        console.log("filterMutations", name, value);

        const doTheFiltering = () => {
            const filtered = this.props.mutations;
            filtered.forEach((mutation) => {
                mutation['filteredByAlsoClosed'] = false;
                mutation['filteredByEreignis'] = true;
                mutation['filteredByGender'] = true;
                mutation['filteredByPerson'] = true;


                if (this.state.personSearchFilterMutations !== "") {
                    const firstNameMatched = () => {
                        console.log('wil lreturn ', this.state.personSearchFilterMutations, mutation.name, RegExp(this.state.personSearchFilterMutations, 'g').test(mutation['name']) );
                        return RegExp( this.state.personSearchFilterMutations, 'gi' ).test( mutation['name'] );
                    }
                    const nameMatched = () => {
                        console.log('wil lreturn ', this.state.personSearchFilterMutations, mutation.surname, RegExp(this.state.personSearchFilterMutations, 'g').test(mutation['surname']) );
                        return RegExp( this.state.personSearchFilterMutations, 'gi' ).test( mutation['surname'] );
                    }
                    const socialNoMatched = () => {
                        console.log('wil lreturn ', this.state.personSearchFilterMutations, mutation.title, RegExp(this.state.personSearchFilterMutations, 'g').test(mutation['title']) );
                        return RegExp( this.state.personSearchFilterMutations, 'gi' ).test( mutation['title'] );
                    }

                    mutation['filteredByPerson'] = nameMatched() || firstNameMatched() || socialNoMatched();
                };

                if (this.state.countryFilterMutations !== "") {
                    mutation['filteredByEreignis'] = (mutation.country === this.state.countryFilterMutations)
                };

                if (this.state.genderFilterMutations !== "") {
                    mutation['filteredByGender'] = (mutation.gender === this.state.genderFilterMutations)
                };

                mutation['filteredByAlsoClosed'] = (mutation.m_oProcessingState !== 0)
                if (this.state.alsoClosedMutationsFilter === true) {
                    mutation['filteredByAlsoClosed'] = (mutation.m_oProcessingState === 0 || mutation.m_oProcessingState === 1)
                }
            });

            this.setState({filteredRowsMutations: filtered});
        }

        if (name === "genderSelectMutations") {
            this.setState({genderFilterMutations: value}, () => doTheFiltering());
        } else if (name === "personSearchMutation") {
            this.setState({personSearchFilterMutations: value}, () => doTheFiltering())
        } else if (name === "countrySelectMutations") {
            this.setState({countryFilterMutations: value}, () => doTheFiltering())
        } else if (name === "alsoClosedCheckboxMutations") {
            this.setState({alsoClosedMutationsFilter: value}, () => doTheFiltering());
        } else {
            console.log("else, will clear all");
            this.setState({
                countryFilterMutations: "",
                alsoClosedMutationsFilter: false,
                personSearchFilterMutations: "",
                genderFilterMutations: ""
            }, () => doTheFiltering());
        }
    }

    getModalName(eventName) {
        if (eventName === "Other") {
            return ""
        } else if (eventName === "Lohnaenderung") {
            return "Beschäftigungsgrad/Lohn"
        }
    }

    filterPersons(name, value) {
        console.log("filterPersons", name, value);

        const doTheFiltering = () => {
            console.log("do the filtering", this.state.firmaFilter);
            const filtered = this.props.all.Data;
            filtered.forEach((person) => {
                person['filteredByInactive'] = true;
                person['filteredByChanged'] = true;
                person['filteredByFirma'] = true;
                person['filteredByPerson'] = true;

                if (this.state.firmaFilter !== "") {
                    person['filteredByFirma'] = (person.m_sEmployerName === this.state.firmaFilter)

                };

                if (this.state.personSearchFilter !== "") {
                    const firstNameMatched = () => {
                        console.log('wil lreturn ', this.state.personSearchFilter, person.m_sFirstname, RegExp(this.state.personSearchFilter, 'g').test(person['m_sFirstname']) );
                        return RegExp( this.state.personSearchFilter, 'gi' ).test( person['m_sFirstname'] );
                    }
                    const nameMatched = () => {
                        console.log('wil lreturn ', this.state.personSearchFilter, person.m_sName, RegExp(this.state.personSearchFilter, 'g').test(person['m_sFirstname']) );
                        return RegExp( this.state.personSearchFilter, 'gi' ).test( person['m_sName'] );
                    }
                    const socialNoMatched = () => {
                        console.log('wil lreturn ', this.state.personSearchFilter, person.m_sSocialSecurityNumber, RegExp(this.state.personSearchFilter, 'g').test(person['m_sFirstname']) );
                        return RegExp( this.state.personSearchFilter, 'gi' ).test( person['m_sSocialSecurityNumber'] );
                    }
                    const birthMatcheed = () => {
                        console.log('wil lreturn ', this.state.personSearchFilter, person.m_dBirthDate, RegExp(this.state.personSearchFilter, 'g').test(person['m_sFirstname']) );
                        return RegExp( this.state.personSearchFilter, 'gi' ).test( person['m_dBirthDate'] );
                    }
                    person['filteredByPerson'] = nameMatched() || firstNameMatched() || socialNoMatched() || birthMatcheed();
                };

                if (this.state.onlyChangedFilter) {
                    person['filteredByChanged'] = (person.m_sEmployeeStatus === 'geändert')
                }

                if (this.state.alsoInactiveFilter) {
                    person['filteredByInactive'] = true;
                } else {
                    person['filteredByInactive'] = (person.m_sEmployeeStatus !== 'Inaktiv')
                }

            });

            this.setState({filteredRowsPersons: filtered});
        }

        if (name === "alsoInactiveCheckbox") {
            this.setState({alsoInactiveFilter: value}, () => doTheFiltering());
        } else if (name === "onlyChangedChekbox") {
            this.setState({onlyChangedFilter: value}, () => doTheFiltering())
        } else if (name === "firmaSelect") {
            this.setState({firmaFilter: value}, () => doTheFiltering())
        } else if (name === "personSearch") {
            this.setState({personSearchFilter: value}, () => doTheFiltering());
        } else {
            console.log("else, will clear all");
            this.setState({
                alsoInactiveFilter: false,
                onlyChangedFilter: false,
                personSearchFilter: "",
                firmaFilter: ""
            }, () => doTheFiltering());
        }
    }


    determineRowClasses(personen, row, existingClasses) {
        let classes = [];
        if (personen) {
            classes = [
                row.filteredByGender === false && 'd-none',
                row.filteredByChanged === false && 'd-none',
                row.filteredByInactive === false && 'd-none',
                row.filteredByPerson === false && 'd-none'
            ];
        } else {
            classes = [
                row.filteredByGender === false && 'd-none',
                row.filteredByEreignis === false && 'd-none',
                row.filteredByAlsoClosed === false && 'd-none',
                row.filteredByPerson === false && 'd-none'
            ];
        }
        // console.log("classes: ", row.filteredByFirma, row.filteredByStatus, row.m_sEmployerName ,classes);
        // console.log("determineRowClasses: ", version, classes.some(cl => cl === 'd-none') ? existingClasses + ' d-none' : existingClasses);
        return classes.some(cl => cl === 'd-none') ? existingClasses + ' d-none' : existingClasses;
    }


    renderPersonenTab() {
        return (<div className="row mt-48">
            <div className="headerRow">

                <div className={classNames('column title', 'col1')}>
                    Name
                </div>
                <div className={classNames('column title', 'col2')}>
                    Geburtstag
                </div>
                <div className={classNames('column title', 'col3')}>
                    AHV-Nummer
                </div>
                <div className={classNames('column title', 'col4')}>
                    Lohn
                </div>
                <div className={classNames('column title', 'col5')}>
                    Status
                </div>
                <div className={classNames('column title', 'col6 icon')}></div>
                <div className={classNames('column title', 'col7 icon')}>
                    <div className="iconWrapper pointer d-none d-xl-block filterIcon" style={{
                            position: 'absolute',
                            right: '32px'
                        }} onClick={(e) => {
                            e.stopPropagation();
                            this.setState({
                                filtersOpened: !this.state.filtersOpened
                            }, () => {
                                setTimeout(() => {
                                    (document.querySelector("input[name='personSearch']") as any).focus();
                                }, 500);
                            })
                        }}>
                        <IconFilter/>
                    </div>
                </div>

            </div>

            {
                this.state.filteredRowsPersons.map((entry, idx) => {
                    return <ExpandableRow
                    className={this.determineRowClasses(true, entry, "")}
                    rowChanged={(expanded) => {this.rowChanged(expanded)}}
                    key={'invoiceRow' + idx}
                    col1={entry.m_sName + ' ' + entry.m_sFirstname} col2={this.parseDate(entry.m_dBirthDate)}
                    col3={entry.m_sSocialSecurityNumber}
                    col4={<div className = "d-flex flex-row showChildOnHover" >
                            <div className="">{entry.m_nGrossSalary}</div>
                            <div className="iconWrapper small ml-3 pointer showable"
                            style={{}}
                            onClick={() => {
                               this.openModal(entry.m_sSocialSecurityNumber, "Beschäftigungsgrad/Lohn");
                            }}>
                                <IconPen/>
                            </div>
                        </div>}
                    col5={this.renderState(entry.m_sEmployeeStatus)}
                    col6={
                        <div className = "iconWrapper pointer" style = {{}}onClick = {() => this.openModal(entry.m_sSocialSecurityNumber, "")} ><IconEdit/>
                        </div>
                        }
                    moreContent={this.renderMoreContentPersonen(entry)}/>
                })
            }

        </div>)
    }

    renderMutationenTab() {
        return (<div className="row mt-48">

            <div className="headerRow">

                <div className={classNames('column title', 'col1')}>
                    Name & Surname
                </div>
                <div className={classNames('column title', 'col2')}>
                    Title
                </div>
                <div className={classNames('column title', 'col3')}>
                    Gender
                </div>
                <div className={classNames('column title', 'col4')}>
                    Country
                </div>
                {/* <div className={classNames('column title', 'col3')}>
                    AHV-Nummer
                </div>
                <div className={classNames('column title', 'col4')}>
                    Ereignis
                </div>
                <div className={classNames('column title', 'col5')}>
                    Bearbeiter
                </div> */}
                <div className={classNames('column title', 'col6 icon')}></div>
                <div className={classNames('column title', 'col7 icon')}>
                    <div className="iconWrapper pointer d-none d-xl-block filterIcon" style={{
                            position: 'absolute',
                            right: '32px'
                        }} onClick={(e) => {
                            e.stopPropagation();
                            this.setState({
                                filtersOpened: !this.state.filtersOpened
                            }, () => {
                                setTimeout(() => {
                                    (document.querySelector("input[name='personSearchMutation']") as any).focus();
                                }, 500);
                            })
                        }}>
                        <IconFilter/>
                    </div>
                </div>

            </div>

            {
                this.props.mutations.map((entry, idx) => {
                    return <ExpandableRow
                        className={this.determineRowClasses(false, entry, "")}
                        key={'insurancesRow' + idx}
                        col1={entry.name + ' ' + entry.surname}
                        col2={entry.title}
                        col3={entry.gender}
                        col4={entry.country}
                        // col4={entry.m_sEventType}
                        // col5={<div className = "d-flex flex-column" > <div className="entity">{entry.m_sUserId}</div>
                        //         <div className="dateString">
                        //             {this.parseDate(entry.m_oTimeStamp) + ' - ' + this.parseDate(entry.m_oTimeStamp, 'hr-min')}
                        //         </div>
                        //     </div>}
                        col6={<div className = "iconWrapper pointer" style = {{}}onClick = {
                                () => this.openModal(entry.name, this.getModalName(entry.name))
                            } > <IconEdit/>
                        </div>}
                        col7={<div className = "iconWrapper large" style = {{}} > <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer">
                                <IconPdfDownload/>
                            </a>
                    </div>}/>
                })
            }
        </div>)
    }

    renderPersonenSidebar() {
        return (<div>
            <div className="paddingContainer">
                <p className="colorGrey mt-5 mb-1">Personen suchen</p>
                <Input
                    id="personSearch"
                    name="personSearch"
                    inputWrapperClass="w-100"
                    placeholder="Name, Geburtstag, AHV-Nr."
                    value={this.state.personSearchFilter}
                    onChange={(name, value) => this.filterPersons(name, value)}
                    onDropdownOpen={(opened) => {
                        console.log("onDropdownOpen", opened)
                    }}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}>
                </Input>
                <Input
                    id="firmaSelect"
                    name="firmaSelect"
                    inputWrapperClass="w-100"
                    placeholder="Firma wahlen"
                    value={this.state.firmaFilter}
                    onChange={(name, value) => this.filterPersons(name, value)}
                    onDropdownOpen={(opened) => {
                            console.log("onDropdownOpen", opened)
                        }}
                    selectOptions={firmaOptions}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}>
                </Input>
                <Input
                    name="onlyChangedChekbox"
                    checked={this.state.onlyChangedFilter}
                    checkboxLabel="Nur geänderte zeigen"
                    onChange={(name, value) => this.filterPersons(name, value)}
                    type='checkbox'
                    inputWrapperClass=""
                    inputWrapperStyle={{
                        display: 'inline-block',
                        width: 'auto',
                        marginTop: '12px',
                        height: '30px'
                    }}>
                </Input>
                <Input
                    checked={this.state.alsoInactiveFilter}
                    checkboxLabel="Auch inaktive zeigen"
                    name="alsoInactiveCheckbox"
                    onChange={(name, value) => this.filterPersons(name, value)}
                    type='checkbox'
                    inputWrapperClass=""
                    inputWrapperStyle={{
                        display: 'inline-block',
                        width: 'auto',
                        height: '30px',
                        marginTop: '0',
                        marginBottom: '18px'
                    }}>
                </Input>
                <button className="applyFilters nonButton ml-auto d-block text-uppercase" onClick={() => this.filterPersons('clear', 'clear')}>
                    Zurücksetzen
                </button>
            </div>
            <div className="breaker"></div>

            <div className="paddingContainer">
                <p className="colorGrey mb-5" style={{
                        marginTop: '50px'
                    }}>PDF generieren</p>
                <div className="d-flex" style={{
                        height: '40px'
                    }}>
                    <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer" className="w-100 d-flex">
                        <p className="colorMain font-14">Versichertenliste</p>
                        <div className="iconWrapper small ml-auto w-auto" style={{
                                height: '20px',
                                marginTop: '-5px'
                            }}>
                            <IconPdfDownload className="" style={{
                                    height: '25px'
                                }}/>
                        </div>
                    </a>

                </div>
                <div className="d-flex" style={{
                        height: '40px'
                    }}>
                    <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer" className="w-100 d-flex">

                        <p className="colorMain font-14">Beitragsliste</p>
                        <div className="iconWrapper small ml-auto w-auto" style={{
                                height: '20px',
                                marginTop: '-5px'
                            }}>
                            <IconPdfDownload className="" style={{
                                    height: '25px'
                                }}/>
                        </div>
                    </a>
                </div>
                <div className="d-flex" style={{
                        height: '40px'
                    }}>
                    <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer" className="w-100 d-flex">
                        <p className="colorMain font-14">Versicherungsausweise</p>
                        <div className="iconWrapper small ml-auto w-auto" style={{
                                height: '20px',
                                marginTop: '-5px'
                            }}>
                            <IconPdfDownload className="" style={{
                                    height: '25px'
                                }}/>
                        </div>
                    </a>
                </div>
            </div>
        </div>)
    }

    renderMutationsSidebar() {
        return (<div>
            <div className="paddingContainer">
                <p className="colorGrey mt-5 mb-1">Personen suchen</p>
                <Input
                    id="personSearchMutation"
                    name="personSearchMutation"
                    inputWrapperClass="w-100"
                    placeholder="Name, surname, title"
                    value={this.state.personSearchFilterMutations}
                    onChange={(name, value) => this.filterMutations(name, value)}
                    onDropdownOpen={(opened) => {
                        console.log("onDropdownOpen", opened)
                    }}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}></Input>
                <Input
                    id="countrySelectMutations"
                    name="countrySelectMutations"
                    inputWrapperClass="w-100"
                    placeholder="Select country"
                    value={this.state.countryFilterMutations}
                    onChange={(name, value) => this.filterMutations(name, value)}
                    onDropdownOpen={(opened) => {
                        console.log("onDropdownOpen", opened)
                    }}
                    selectOptions={countryOptionsSelectForFilter}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}></Input>
                <Input
                    id="genderSelectMutations"
                    name="genderSelectMutations"
                    inputWrapperClass="w-100"
                    placeholder="Select gender"
                    value={this.state.genderFilterMutations}
                    onChange={(name, value) => this.filterMutations(name, value)}
                    onDropdownOpen={(opened) => {
                        console.log("onDropdownOpen", opened)
                    }}
                    selectOptions={genderOptionsSelectForFilter}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}></Input>
                <Input
                    id="alsoClosedCheckboxMutations"
                    name="alsoClosedCheckboxMutations"
                    checked={this.state.alsoClosedMutationsFilter}
                    checkboxLabel="Auch abgeschlossene Mutationen zeigen"
                    onChange={(name, value) => this.filterMutations(name, value)}
                    type='checkbox'
                    inputWrapperClass=""
                    inputWrapperStyle={{
                        display: 'inline-block',
                        width: 'auto',
                        marginTop: '12px',
                        height: '30px'
                    }}></Input>
                <button className="applyFilters nonButton ml-auto d-block text-uppercase" onClick={() => this.filterMutations('clear', 'clear')}>
                    Zurücksetzen
                </button>
            </div>
            <div className="breaker"></div>

            <div className="paddingContainer">
                <p className="colorGrey mb-5" style={{
                        marginTop: '50px'
                    }}>PDF generieren</p>
                <div className="d-flex" style={{
                        height: '40px'
                    }}>
                    <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer" className="w-100 d-flex">
                        <p className="colorMain font-14">Liste als PDF herunterladen</p>
                        <div className="iconWrapper small ml-auto w-auto" style={{
                                height: '20px',
                                marginTop: '-5px'
                            }}>
                            <IconPdfDownload className="" style={{
                                    height: '25px'
                                }}/>
                        </div>
                    </a>
                </div>

            </div>
        </div>)
    }

    renderNewEntryStep() {
        const genderOptions = [
          {
              id: '1',
              label: 'Männlich',
              validationMessageConfig: null,
              validationTests: [EValidationTestType.hasValue],
              type: 'radio',
              name: "genderRadios",
              inputWrapperClass: "w-100 high",
              value: this.state.eintrittErfassenForm.gender,
              onChange: this.handleRadioChange
          }, {
              id: '2',
              label: 'Weiblich',
              validationMessageConfig: null,
              validationTests: [EValidationTestType.hasValue],
              type: 'radio',
              name: "genderRadios",
              inputWrapperClass: "w-100 high",
              value: this.state.eintrittErfassenForm.gender,
              onChange: this.handleRadioChange
          }
        ]

        return renderSwitch(this.state.newEntryStep, {
          1: () => (
              <div className="row">
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-0 mb-1">Anrede</p>
                      <Input
                        id="anredeInput"
                        name="anredeInput"
                        inputWrapperClass="w-100"
                        placeholder="Herr, Frau"
                        label="Anrede"
                        value={this.state.eintrittErfassenForm.salutation}
                        onChange={(name, value) => this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, salutation: value}})}
                        onDropdownOpen={(opened) => {
                              console.log("onDropdownOpen", opened)
                          }}
                        selectOptions={salutationOptions}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]} inputWrapperStyle={{}}>
                      </Input>
                  </div>

                  <div className="col-12 col-sm-6"></div>

                  <div className="col-12 col-sm-6">
                      <Input
                        id="vornameInput"
                        name="vorname"
                        inputWrapperClass="w-100 high"
                        value={this.state.lohnAnpassenForm.vorname}
                        onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, vorname: value}})}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]}
                        inputWrapperStyle={{}}>
                        Vorname
                      </Input>
                  </div>

                  <div className="col-12 col-sm-6">
                      <Input
                        id="nameInput"
                        name="name"
                        inputWrapperClass="w-100 high"
                        value={this.state.lohnAnpassenForm.name}
                        onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, name: value}})}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]}
                        inputWrapperStyle={{}}>
                        Name
                      </Input>
                  </div>

                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-5 mb-1 labelReplacement">Geburtsdatum</p>
                      <Input
                        id="birthInput"
                        name="birthday"
                        inputWrapperClass="w-100"
                        value={this.state.lohnAnpassenForm.birthday}
                        onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, birthday: value}})}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='date'
                        validationTests={[EValidationTestType.hasValue]}
                        inputWrapperStyle={{marginTop: '7px'}}>
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6">
                      <Input
                        id="ahvNummerInput"
                        name="ahvNummer"
                        inputWrapperClass="w-100 high"
                        value={this.state.lohnAnpassenForm.ahvNummer}
                        onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, ahvNummer: value}})}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]}
                        inputWrapperStyle={{}}>
                        AHV-Nummer
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-5 mb-4">Geschlecht</p>
                      <Input
                        id="genderRadios"
                        radioOptions={genderOptions}
                    >
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-5 mb-4">Korrespondenzsprache</p>
                      <Input
                        id="languageRadios"
                        radioOptions={this.languageOptions()}
                    >
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-md-6  modalNavigationContainer">
                      <Button
                        className="text-uppercase col-md-5 font-14 ml-auto cancelButton"
                        onClick={this.closeModal}
                        to={null}
                        buttonType={EButtonType.BasicLink}
                      >
                        Abbrechen
                      </Button>
                      <Button
                        className="text-uppercase col-md-5 nextButton"
                        submit={true}
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep + 1}) } }
                        buttonType={EButtonType.FullWidth}
                        >
                        WEITER
                      </Button>
                  </div>
              </div>
          ),
          2: () => (
              <div className="row">
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-0 mb-1">Anrede</p>
                      <Input
                        id="anredeInput"
                        name="anredeInput"
                        inputWrapperClass="w-100"
                        placeholder="Herr, Frau"
                        label="Anrede"
                        value={this.state.eintrittErfassenForm.salutation}
                        onChange={(name, value) => this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, salutation: value}})}
                        onDropdownOpen={(opened) => {
                              console.log("onDropdownOpen", opened)
                          }}
                        selectOptions={salutationOptions}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]} inputWrapperStyle={{}}>
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6 d-flex">
                      <Button
                        className="text-uppercase col-5 font-14 ml-auto"
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep - 1}) }}
                        to={null}
                        buttonType={EButtonType.BasicLink}
                      >
                        zurück
                      </Button>
                      <Button
                        className="text-uppercase col-5"
                        submit={true}
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep + 1}) } }
                        buttonType={EButtonType.FullWidth}
                        >
                        WEITER
                      </Button>
                  </div>
              </div>
          ),
          3: () => (
              <div className="row">
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-0 mb-1">Anrede</p>
                      <Input
                        id="anredeInput"
                        name="anredeInput"
                        inputWrapperClass="w-100"
                        placeholder="Herr, Frau"
                        label="Anrede"
                        value={this.state.eintrittErfassenForm.salutation}
                        onChange={(name, value) => this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, salutation: value}})}
                        onDropdownOpen={(opened) => {
                              console.log("onDropdownOpen", opened)
                          }}
                        selectOptions={salutationOptions}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]} inputWrapperStyle={{}}>
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6 d-flex">
                      <Button
                        className="text-uppercase col-5 font-14 ml-auto"
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep - 1}) }}

                        to={null}
                        buttonType={EButtonType.BasicLink}
                      >
                        zurück
                      </Button>
                      <Button
                        className="text-uppercase col-5"
                        submit={true}
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep + 1}) } }
                        buttonType={EButtonType.FullWidth}
                        >
                        WEITER
                      </Button>
                  </div>
              </div>
          ),
          4: () => (
              <div className="row">
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-0 mb-1">Anrede</p>
                      <Input
                        id="anredeInput"
                        name="anredeInput"
                        inputWrapperClass="w-100"
                        placeholder="Herr, Frau"
                        label="Anrede"
                        value={this.state.eintrittErfassenForm.salutation}
                        onChange={(name, value) => this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, salutation: value}})}
                        onDropdownOpen={(opened) => {
                              console.log("onDropdownOpen", opened)
                          }}
                        selectOptions={salutationOptions}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]} inputWrapperStyle={{}}>
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6 d-flex">
                      <Button
                        className="text-uppercase col-5 font-14 ml-auto"
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep - 1}) }}
                        to={null}
                        buttonType={EButtonType.BasicLink}
                      >
                        zurück
                      </Button>
                      <Button
                        className="text-uppercase col-5"
                        submit={true}
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep + 1}) } }
                        buttonType={EButtonType.FullWidth}
                        >
                        WEITER
                      </Button>
                  </div>
              </div>
          ),
          5: () => (
              <div className="row">
                  <div className="col-12 col-sm-6">
                      <p className="colorGrey mt-0 mb-1">Anrede</p>
                      <Input
                        id="anredeInput"
                        name="anredeInput"
                        inputWrapperClass="w-100"
                        placeholder="Herr, Frau"
                        label="Anrede"
                        value={this.state.eintrittErfassenForm.salutation}
                        onChange={(name, value) => this.setState({eintrittErfassenForm: {...this.state.eintrittErfassenForm, salutation: value}})}
                        onDropdownOpen={(opened) => {
                              console.log("onDropdownOpen", opened)
                          }}
                        selectOptions={salutationOptions}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        validationTests={[EValidationTestType.hasValue]} inputWrapperStyle={{}}>
                      </Input>
                  </div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6"></div>
                  <div className="col-12 col-sm-6 d-flex">
                      <Button
                        className="text-uppercase col-5 font-14 ml-auto"
                        onClick={() => { this.setState({newEntryStep: this.state.newEntryStep - 1}) }}
                        to={null}
                        buttonType={EButtonType.BasicLink}
                      >
                        zurück
                      </Button>
                      <Button
                        className="text-uppercase col-7"
                        submit={true}
                        onClick={() => { this.openModal(this.state.modalContentId, 'success'); } }
                        buttonType={EButtonType.FullWidth}
                        >
                        Bestätigen
                      </Button>
                  </div>
              </div>
          ),
        })
    }

    renderModalContent(content) {
        console.log("renderModalContent", content);

        const genderOptions = [
          {
              id: '1',
              label: 'Männlich',
              validationMessageConfig: null,
              validationTests: [EValidationTestType.hasValue],
              type: 'radio',
              name: "genderRadios",
              inputWrapperClass: "w-100 high",
              value: this.state.eintrittErfassenForm.gender,
              onChange: this.handleRadioChange
          }, {
              id: '2',
              label: 'Weiblich',
              validationMessageConfig: null,
              validationTests: [EValidationTestType.hasValue],
              type: 'radio',
              name: "genderRadios",
              inputWrapperClass: "w-100 high",
              value: this.state.eintrittErfassenForm.gender,
              onChange: this.handleRadioChange
          }
        ]

      return renderSwitch(content, {
        "": () => (
          <div className="row">

              <div className="col-12 col-sm-6 col-xl-4 mb-5">
                  <p className="title colorGrey">Personalien ändern</p>
                  <Button
                      className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Versicherteninfos+bearbeiten`}
                      buttonType={EButtonType.BasicLink}>
                      Versicherteninfos
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Adresse`}
                      buttonType={EButtonType.BasicLink}>
                      Adresse
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Kontaktinformationen`}
                      buttonType={EButtonType.BasicLink}>
                      Kontaktinformationen
                  </Button>
              </div>

              <div className="col-12 col-sm-6 col-xl-4 mb-5">
                  <p className="title colorGrey">Beschäftigung ändern</p>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Beschäftigungsgrad/Lohn`}
                      buttonType={EButtonType.BasicLink}>
                      Beschäftigungsgrad/Lohn
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Unbezahlter+Urlaub`}
                      buttonType={EButtonType.BasicLink}>
                      Unbezahlter Urlaub
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Austritt`}
                      buttonType={EButtonType.BasicLink}>
                      Austritt
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Erwerbsunfähigkeit`}
                      buttonType={EButtonType.BasicLink}>
                      Erwerbsunfähigkeit
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Invalidität`}
                      buttonType={EButtonType.BasicLink}>
                      Invalidität
                  </Button>
              </div>

              <div className="col-12 col-sm-6 col-xl-4 mb-5">
                  <p className="title colorGrey">Ereignisse ändern</p>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Zivilstand`}
                      buttonType={EButtonType.BasicLink}>
                      Zivilstand
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Pensionierung`}
                      buttonType={EButtonType.BasicLink}>
                      Pensionierung
                  </Button>
                  <Button className="colorMain w-auto buttonLink d-block text-uppercase font-14 text-left" onClick={() => {
                          console.log("password forgot clicked")
                      }}
                      to={`/versicherte/personen?edit=${this.state.modalContentId}&content=Todesfall`}
                      buttonType={EButtonType.BasicLink}>
                      Todesfall
                  </Button>
              </div>
          </div>
        ),
        "Beschäftigungsgrad/Lohn": () => (
          <div className="row">
              <div className="col-12 col-sm-6">
                    <Input
                      id="inputId"
                      name="lohn"
                      inputWrapperClass="w-100 high"
                      value={this.state.lohnAnpassenForm.lohn}
                      onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, lohn: value}})}
                      validationMessageConfig={null/* this.validationMessageConfig() */}
                      type='text'
                      validationTests={[EValidationTestType.hasValue]}
                      inputWrapperStyle={{}}>
                      Lohn
                    </Input>
              </div>

              <div className="col-12 col-sm-6">
                  <Input
                    id="inputId"
                    name="lohn"
                    label="label"
                    inputWrapperClass="w-100 high"
                    value={this.state.lohnAnpassenForm.lohn}
                    onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, lohn: value}})}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}>
                    Pensum in %
                  </Input>
              </div>

              <div className="col-12 col-sm-6">
                  <Input
                    id="inputId"
                    name="lohn"
                    label="label"
                    inputWrapperClass="w-100 high"
                    value={this.state.lohnAnpassenForm.lohn}
                    onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, lohn: value}})}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}>
                    Bonus
                  </Input>
              </div>

              <div className="col-12 col-sm-6">
                  <Input
                    id="inputId"
                    name="lohn"
                    label="label"
                    inputWrapperClass="w-100 high"
                    value={this.state.lohnAnpassenForm.lohn}
                    onChange={(name, value) => this.setState({lohnAnpassenForm: {...this.state.lohnAnpassenForm, lohn: value}})}
                    validationMessageConfig={null/* this.validationMessageConfig() */}
                    type='text'
                    validationTests={[EValidationTestType.hasValue]}
                    inputWrapperStyle={{}}>
                    Gültig ab
                  </Input>
              </div>
              <div className="col-12 col-sm-6">
              </div>
              <div className="col-12 col-md-6 modalNavigationContainer">
              <Button
                className="text-uppercase col-md-5 font-14 cancelButton"
                onClick={this.closeModal}
                to={null}
                buttonType={EButtonType.BasicLink}
              >
                Abbrechen
              </Button>
              <Button
                className="text-uppercase col-md-7 nextButton"
                submit={true}
                onClick={() => { this.openModal(this.state.modalContentId, 'success'); } }
                buttonType={EButtonType.FullWidth}
                >
                Bestätigen
              </Button>
              </div>
          </div>
        ),
        "Eintritt erfassen": () => (
          this.renderNewEntryStep()
      ),
      "Versicherteninfos bearbeiten": () => (
        <div className="row">
            <div className="col-12 col-sm-6">
                <p className="colorGrey mt-0 mb-1">Anrede</p>
                <Input
                  id="anredeInput"
                  name="anredeInput"
                  inputWrapperClass="w-100"
                  placeholder="Herr, Frau"
                  label="Anrede"
                  value={this.state.versicherteInfosForm.salutation}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, salutation: value}})}
                  onDropdownOpen={(opened) => {
                        console.log("onDropdownOpen", opened)
                    }}
                  selectOptions={salutationOptions}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]} inputWrapperStyle={{}}>
                </Input>
            </div><div className="col-12 col-sm-6"></div>

            <div className="col-12 col-sm-6">
                <Input
                  id="vornameInput"
                  name="vornameInput"
                  inputWrapperClass="w-100 high"
                  value={this.state.lohnAnpassenForm.vorname}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, vorname: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                  Vorname
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <Input
                  id="nameInput"
                  name="nameInput"
                  inputWrapperClass="w-100 high"
                  value={this.state.lohnAnpassenForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                  Name
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <p className="colorGrey mt-5 mb-4">Geschlecht</p>
                <Input
                  id="genderRadios"
                  radioOptions={genderOptions}
              >
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <p className="colorGrey mt-5 mb-1 labelReplacement">Geburtsdatum</p>
                <Input
                  id="nameInput"
                  name="nameInput"
                  inputWrapperClass="w-100"
                  value={this.state.lohnAnpassenForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='date'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{marginTop: '7px'}}>
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <Input
                  id="ahvInput"
                  name="ahvInput"
                  inputWrapperClass="w-100 high"
                  value={this.state.lohnAnpassenForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                  AHV-Nummer
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <Input
                  id="ahvInput"
                  name="ahvInput"
                  inputWrapperClass="w-100 high"
                  value={this.state.lohnAnpassenForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                  Personalnummer
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <p className="colorGrey mt-5 mb-4">Korrespondenzsprache</p>
                <Input
                  id="languageRadios"
                  radioOptions={this.languageOptions()}
              >
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <Input
                  id="ahvInput"
                  name="ahvInput"
                  inputWrapperClass="w-100 high"
                  value={this.state.versicherteInfosForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                  Kostenstelle
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <Input
                  id="ahvInput"
                  name="ahvInput"
                  inputWrapperClass="w-100 high"
                  value={this.state.versicherteInfosForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='text'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                  Versicherungsplan
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <p className="colorGrey mb-1 labelReplacement">Eintrittsdatum Versicherung</p>
                <Input
                  id="ahvInput"
                  name="ahvInput"
                  inputWrapperClass="w-100"
                  value={this.state.versicherteInfosForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='date'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                </Input>
            </div>

            <div className="col-12 col-sm-6">
                <p className="colorGrey mb-1 labelReplacement">Austrittsdatum Versicherung</p>
                <Input
                  id="ahvInput"
                  name="ahvInput"
                  inputWrapperClass="w-100"
                  value={this.state.versicherteInfosForm.name}
                  onChange={(name, value) => this.setState({versicherteInfosForm: {...this.state.versicherteInfosForm, name: value}})}
                  validationMessageConfig={null/* this.validationMessageConfig() */}
                  type='date'
                  validationTests={[EValidationTestType.hasValue]}
                  inputWrapperStyle={{}}>
                </Input>
            </div>

            <div className="col-12 col-sm-6"></div>
            <div className="col-12 col-sm-6"></div>
            <div className="col-12 col-md-6 modalNavigationContainer">
                <Button
                  className="text-uppercase col-md-5 font-14 ml-auto cancelButton"
                  onClick={this.closeModal}
                  to={null}
                  buttonType={EButtonType.BasicLink}
                >
                  Abbrechen
                </Button>
                <Button
                  className="text-uppercase col-md-5 nextButton"
                  submit={true}
                  onClick={() => { console.log("clicked");}}
                  buttonType={EButtonType.FullWidth}
                  >
                  WEITER
                </Button>
            </div>
        </div>
    ),
    "success": () => (
      <div className="row flex-column">
          <div className="mx-auto" style={{maxWidth: '400px'}}>
              <div className="pill large bgGreen mx-auto p-relative mb-5">
                    <IconCheckmarkSlim className="strokeWhite" />
              </div>

              <h1 className="text-center my-4">Eintritt wurde hinzugefügt!</h1>
              <p className="text-center">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.</p>
          </div>
          <div className="mx-auto"
              style={{maxWidth: '180px'}}
          >
              <Button
                className="text-uppercase"
                submit={true}
                onClick={this.closeModal}
                buttonType={EButtonType.FullWidth}
                >
                Zur Übersicht
              </Button>
          </div>
      </div>
        )
    }, () => (<div className="row flex-column">
        <div className="mx-auto" style={{maxWidth: '400px'}}>
            <h1 className="text-center my-4">TBD</h1>
            <p className="text-center">During integration pahse</p>
        </div>
        <div className="mx-auto"
            style={{maxWidth: '180px'}}
        >
            <Button
              className="text-uppercase"
              submit={true}
              onClick={() => this.props.history.goBack()}
              buttonType={EButtonType.FullWidth}
              >
              zurück
            </Button>
        </div>
    </div>))
    }

    renderModal() {
        return (
          <div className="modalContent">
            <div className="modalHeader">
                {this.state.modalContent !== 'success' && <Button className="colorGrey w-auto mt-0 d-block text-uppercase text-left font-14" onClick={this.closeModal} to={null} buttonType={EButtonType.BasicLink}>
                    <div className="iconWrapper arrow left d-inline-block mr-3 small">
                        <IconArrowRight />
                    </div>
                    Zurück zur Übersicht
                </Button>}
                {(this.state.modalContent !== 'Eintritt erfassen' && this.state.modalContent !== 'success') &&
                <div className="ml-md-auto ml-0 mt-5 mt-md-0 d-flex">
                  <div className='column' style={{minWidth: '130px'}}>
                    {this.props.all.Data[0]['m_sName'] + ' ' + this.props.all.Data[0]['m_sFirstname']}
                  </div>
                  <div className="column ml-4">
                      {this.parseDate(this.props.all.Data[0]['m_dBirthDate'])}
                  </div>
                  <div className="column ml-4">
                      {this.props.all.Data[0]['m_sSocialSecurityNumber']}
                  </div>
                  <div className="column ml-4">
                      {this.props.all.Data[0]['m_nGrossSalary']}
                  </div>
              </div>
              }

            </div>
            <div className="modalSubHeader d-flex">
                {this.state.modalContent !== 'success' && <h1 ref={subtitle => this.subtitle = subtitle} className="d-inline-block">{this.state.modalContent ? this.state.modalContent : "Was möchten Sie ändern?"}</h1>}
                {this.state.modalContent === 'Eintritt erfassen' &&
                    <div className="stepIndicator">
                        <span className={`step ${this.state.newEntryStep === 1 && 'active'}`}>
                            <div className='pill'>
                              1
                            </div>Personalien
                        </span>
                        <span className={`step ${this.state.newEntryStep === 2 && 'active'}`}>
                            <div className='pill'>
                              2
                            </div>Versicherung
                        </span>
                        <span className={`step ${this.state.newEntryStep === 3 && 'active'}`}>
                            <div className='pill'>
                              3
                            </div>LOHN
                        </span>

                        <span className={`step ${this.state.newEntryStep === 4 && 'active'}`}>

                            <div className='pill'>
                              4
                            </div>ADRESSE
                        </span>
                        <span className={`step ${this.state.newEntryStep === 5 && 'active'}`}>
                            <div className='pill'>
                              5
                            </div>KONTAKT
                        </span>
                    </div>
                }
            </div>

            <div className="modalContentWarpper">
                <div className="container-fluid px-0">
                        {this.renderModalContent(this.state.modalContent)}
                </div>
            </div>
        </div>)
    }

    activatePersonenTab = () => {
      this.props.history.push({
          pathname: '/versicherte/personen',
          // search: "?" + new URLSearchParams({'tab': '0'}).toString()
      })
      // this.setState({tabActive: 0});
    }

    activateMutationTab = () => {
      this.props.history.push({
          pathname: '/versicherte/mutationen',
          // search: "?" + new URLSearchParams({'tab': '1'}).toString()
      })
      // this.setState({tabActive: 0});
    }

    render() {
        return (<div className="p-relative h-100">
            <Header title="Versicherte">
                <Tab active={this.state.currentTab === 'personen'} minWidth className="mr-5" onClick={this.activatePersonenTab}>
                    <span className='mdc-tab__text-label'>PERSONEN</span>
                </Tab>
                <Tab active={this.state.currentTab === 'mutationen'} minWidth onClick={this.activateMutationTab}>
                    <span className='mdc-tab__text-label'>MUTATIONEN</span>
                </Tab>
                <div className="d-block d-xl-none iconWrapper pointer" style={{
                        position: 'absolute',
                        right: '20px',
                        top: '30px'
                    }} onClick={(e) => {
                        e.stopPropagation();
                        this.setState({
                            filtersOpened: !this.state.filtersOpened
                        })
                    }}>
                    <IconFilter/>
                </div>
            </Header>

            <Modal isOpen={this.state.modalIsOpen} onAfterOpen={this.afterOpenModal} onRequestClose={this.closeModal} contentLabel="Example Modal" style={{
                    content: {
                        border: 'none',
                        background: 'transparent',
                        right: 0,
                        bottom: 0,
                        top: 0,
                        left: 0
                    }
                }}>
                {this.renderModal()}
            </Modal>

            <div className="contentContainer insurances d-flex" onClick={this.handleContentContainerClick}>
                <div className="container-fluid w-100" style={{}}>
                    <div className="customContainer">
                        <div className="row">
                            {this.state.currentTab === 'personen' && <div className="pointer" onClick={() => {this.openModal("", "Eintritt erfassen"); this.setState({newEntryStep: 1})}}>
                                <div className="addButton">
                                    <div className="iconWrapper"><IconPlus/></div>
                                </div>
                                <span className="d-none d-sm-inline buttonExplanation text-uppercase ml-4">Eintritt Erfassen</span>
                            </div>}
                        </div>

                        {
                            this.state.currentTab === 'personen'
                                ? this.renderPersonenTab()
                                : this.renderMutationenTab()
                        }

                        </div>
                    </div>

                    <div className=""ref={node => this.drawer = node}>
                        <Drawer opened={this.state.filtersOpened} >
                            {/*<Button
                      className="d-inline-block font-14 text-right font-500 mt-2 mb-5"
                      onClick={() => { this.setState({filtersOpened: false}) }}
                      to={null}
                      buttonType={EButtonType.BasicLink}
                    >
                      Einklappen
                    </Button>*/
                            }
                            {
                                this.state.currentTab === 'personen'
                                    ? this.renderPersonenSidebar()
                                    : this.renderMutationsSidebar()
                            }

                        </Drawer>
                    </div>
            </div>
        </div>);
    }
}

function mapStateToProps(state
: RootState, ownProps
: any) {
    return {
        ...ownProps,
        mutations: state.app.mutations,
        all: state.app.all,
        invoices: state.app.invoices
    }
}

const mapDispatchToProps = (dispatch) => ({
    getMutations: () => dispatch(getMutations())
})

export default connect(mapStateToProps, mapDispatchToProps)(Insurances);
