import React from 'react';
import { connect } from 'react-redux';
// import { Link } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { RootState } from '../../reducers/index';
import * as actions from '../../actions/';
import './Login.scss';
import { Input } from '../../components/Input/index';
import { EValidationTestType } from '../../utils/validationConfigs';
import { EValidationMessageType } from '../../components/ValidationMessage/index';
import { Button, EButtonType } from '../../components/Button/index';
// import Spinner from '../../components/Spinner/index';
import { ReactComponent as IconCheckmark } from '../../assets/icons/checkmark-slim.svg';
import { ReactComponent as IconEye } from '../../assets/icons/eye.svg';
import '../../utils/jwtToken';
import logo from '../../assets/imgs/logo.jpg';
import {toastr} from 'react-redux-toastr'

interface State {
  login: string;
  password: string;
  passwordType: string;
  rememberMe: boolean;
}

interface Props {
  actions: any;
}

class Login extends React.Component<Props, State> {
  constructor(props: any) {
    console.log("Login constructor called");
    document.body.classList.add("login");

    super(props);
    this.state = {
      login: "",
      password: "",
      passwordType: "password",
      rememberMe: false
    };
  }

  componentDidMount() {
    console.log('login app did mount', this.props);
  }

  componentWillUnmount() {
    document.body.classList.remove("login");
  }

  private validationMessageConfig() {
    // console.log("validationMessageConfig");
    // if (this.state.isValidationMessage && !this.state.isChecked) {
      return {
        message: 'Please make sure you have read and understood',
        type: EValidationMessageType.Error
      };
    // } else {
    //   return EMPTY_VALIDATION_MESSAGE_CONFIG;
    // }
  }

  login() {
    console.log("login clicked", this.props);
    this.props.actions.login({login: this.state.login, token: this.state.password})
                      .then(res => toastr.success('Success', res))
                      .catch(err => toastr.error('Error', err));
  }

  changePasswordInputType() {
      if (this.state.passwordType === "text") {
          this.setState({
              passwordType: "password"
          })

      } else {
          this.setState({
              passwordType: "text"
          })
      }
  }


  render() {
    return (
      <div className="container-fluid">
        <div className="card topCard d-block d-sm-none">
            <img alt="logo" src={logo} />
        </div>
        <div className="row">
          <div className="card mx-auto text-right" style={{marginTop: "112px"}}>
            <img alt="logo" src={logo} style={{height: '56px', marginBottom: '55px'}} className="d-none d-sm-inline"/>
            <h1 className="p-relative text-left" style={{top: "-6px"}}>PK Web+ Arbeitgeber</h1>
            <form onSubmit={(e) => {console.log("onsubmit", e); e.preventDefault();}}>
            <Input
              value={this.state.login}
              onChange={(name, value) => this.setState({ login: value }) }
              validationMessageConfig={null/*this.validationMessageConfig()*/}
              type='text'
              placeholder="Benutzername"
              inputWrapperClass="high"
              validationTests={[EValidationTestType.isEmail]}
              inputWrapperStyle={{
                "display": 'block',
                "width": 'auto',
                "marginTop": '3.2rem'
              }}
            >
                Benutzername
                {this.state.login.length > 3 && <IconCheckmark style={{position: 'absolute', right: '0', bottom: '-46px'}} className="iconWrapper"/>}
            </Input>
            <Input
              value={this.state.password}
              onChange={(name, value) => this.setState({ password: value }) }
              validationMessageConfig={null/*this.validationMessageConfig()*/}
              type={this.state.passwordType}
              placeholder="Passwort"
              inputWrapperClass="high"
              validationTests={[EValidationTestType.isPassword]}
              inputWrapperStyle={{
                "display": 'block',
                "width": 'auto',
              }}
            >
                Passwort
                <div className="pointer" onClick={() => this.changePasswordInputType()}>
                    <IconEye className="iconWrapper" style={{position: 'absolute', right: '0', bottom: '-46px'}} />
                </div>
            </Input>
            <div className="d-flex text-left" style={{marginTop: '0px'}}>
              <Input
                value={this.state.rememberMe}
                checkboxLabel="Login merken"
                onChange={(name, value) => this.setState({ rememberMe: value }) }
                type='checkbox'
                inputWrapperClass="mb-0"
                inputWrapperStyle={{
                  display: 'flex',
                  width: 'auto',
                  alignItems: 'center'
                }}
              >
              </Input>
              <Button
                className="d-inline-block w-auto mt-0 ml-auto d-none passwordVergessenButton"
                onClick={() => { console.log("password forgot clicked") }}
                to={null}
                buttonType={EButtonType.BasicLink}
              >
                PASSWORD VERGESSEN
              </Button>
            </div>
              <Button
                className="test"
                submit={true}
                onClick={() => { console.log("clicked"); this.login()}}
                buttonType={EButtonType.FullWidth}
                >
                ANMELDEN
              </Button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: RootState, ownProps: any) {
  return {
    ...ownProps,
    ...state.app,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions as any, dispatch)

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
