import React from 'react';
import {connect} from 'react-redux';
import {RootState} from '../../reducers/index';
import {Dispatch} from 'redux';
import * as actions from '../../actions/';
import {Button, EButtonType} from '../../components/Button/index';
import Header from '../../components/Header';
import Mutation from '../../components/Cockpit/Mutation/index';
import Invoice from '../../components/Cockpit/Invoice/index';
// import { WithContext as ReactTags } from 'react-tag-input';
import {EValidationTestType} from '../../utils/validationConfigs';
import {Input} from '../../components/Input/index';
import {ReactComponent as IconArrow} from '../../assets/icons/arrow-right.svg';
import {ReactComponent as IconPdfDownload} from '../../assets/icons/pdfdownload.svg';

import './Cockpit.scss';

const firmaOptions = [
    {
        id: '154',
        dropdownLabel: 'Firma 144'
    }, {
        id: '155',
        dropdownLabel: 'Firma 145'
    }
];


interface State {
    firmaFilter: string;
}

interface Props {
    mutations: Array<any>;
    invoices: Array<any>;
    all: any;
}

class Cockpit extends React.Component<Props, State> {
    constructor(props : any) {
        document.body.classList.add("cockpitPage");

        super(props);
        this.state = {
            firmaFilter: ""
        }
    }

    componentDidMount() {
        console.log('main app did mount', this.props);
        document.body.classList.remove('backdrop');
    }

    componentWillUnmount() {
        document.body.classList.remove("cockpitPage");
    }

    render() {
        return (<div className="">
            <Header title="Cockpit"></Header>
            <div className="contentContainer">
                <div className="customContainer overflowHiddenOff">
                    {/*<ReactTags tags={tags}
                suggestions={suggestions}
                handleDelete={this.handleDelete}
                handleAddition={this.handleAddition}
                handleDrag={this.handleDrag}
                delimiters={delimiters} />*/
                    }
                    <Input
                        id="firmaSelect"
                        name="firmaSelect"
                        placeholder="Firma wählen"
                        value={this.state.firmaFilter}
                        onChange={(name, value) => {
                            console.log("onchange", name, value);
                            this.setState({firmaFilter: value})
                        }}
                        onDropdownOpen={(opened) => {
                            console.log("onDropdownOpen", opened)
                        }}
                        selectOptions={firmaOptions}
                        validationMessageConfig={null/* this.validationMessageConfig() */}
                        type='text'
                        inputWrapperClass="whiteBgDropdown"
                        validationTests={[EValidationTestType.hasValue]}
                        inputWrapperStyle={{
                            width: "auto",
                            height: "56px",
                            display: "flex",
                            justifyContent: "flex-end",
                            marginRight: "48px"
                        }}>
                    </Input>
                </div>
                <div className="customContainer">
                    <div className="container-fluid" style={{
                            marginTop: '51px'
                        }}>
                        <div className="row">

                            <div className="col-lg-4 col-12">
                                <div className="columnContent">
                                    <div className="columnHeader">
                                        <h1>Offene Mutationen</h1>
                                        <p>Sie haben offene, noch nicht an die PK abgegebene Mutationen. Geben Sie die Änderungen ab, um Sie verarbeiten zu lassen.</p>
                                        <Button
                                            className="d-flex text-uppercase font-14 text-left mt-auto bgGreenLight"
                                            onClick={() => {
                                                console.log("password forgot clicked")
                                            }}
                                            href="https://test03.icr.ch/"
                                            buttonType={EButtonType.FullWidth}>
                                            Änderung abgeben
                                            <div className="iconWrapper small ml-auto">
                                                <IconArrow className="strokeWhite fillWhite"/>
                                            </div>
                                        </Button>
                                    </div>
                                    {
                                        this.props.mutations.map((mutation, idx) => {
                                            if (this.state.firmaFilter === "") {
                                                return <Mutation key={'mutation' + idx} changeable={mutation.m_oProcessingState === 1} type={mutation.m_sEventType} owner={`${mutation.m_sFirstname} ${mutation.m_sName}`} no={mutation.m_sSocialSecurityNumber}/>
                                            } else {
                                                if (mutation.m_sEmployerName === this.state.firmaFilter) {
                                                    return <Mutation key={'mutation' + idx} changeable={mutation.m_oProcessingState === 1} type={mutation.m_sEventType} owner={`${mutation.m_sFirstname} ${mutation.m_sName}`} no={mutation.m_sSocialSecurityNumber}/>
                                                } else {
                                                    return null
                                                }
                                            }
                                        })
                                    }
                                    <Button className="d-inline-block text-uppercase font-14 text-center text-sm-left mt-0 mb-5" onClick={() => {
                                            console.log("password forgot clicked")
                                        }} to={'/versicherte/mutationen'} buttonType={EButtonType.BasicLink}>
                                        Alle Mutationen
                                        <div className="iconWrapper small ml-2">
                                            <IconArrow/>
                                        </div>
                                    </Button>
                                </div>
                            </div>

                            <div className="col-lg-4 col-12">
                                <div className="columnContent">
                                    <div className="columnHeader">
                                        <h1>Abgeschlossene Mutationen</h1>
                                        <p>Ihre zuletzt an die PK abgegebenen und bereits verarbeiteten Mutationen.</p>
                                        <Button
                                            className="d-flex text-uppercase font-14 text-left mt-auto"
                                            onClick={() => {
                                                console.log("password forgot clicked")
                                            }}
                                            href="http://www.orimi.com/pdf-test.pdf"
                                            buttonType={EButtonType.FullWidthInverted}
                                        >
                                            Übersichts-PDF herunterladen
                                            <div className="iconWrapper small ml-auto w-auto" style={{
                                                    height: '20px',
                                                    marginTop: '-5px'
                                                }}>
                                                <IconPdfDownload className="" style={{
                                                        height: '25px'
                                                    }}/>
                                            </div>
                                        </Button>
                                    </div>
                                    {
                                        this.props.mutations.map((mutation, idx) => {
                                            if (this.state.firmaFilter === "") {
                                                return <Mutation key={'mutation' + idx} changeable={mutation.m_oProcessingState === 0} type={mutation.m_sEventType} owner={`${mutation.m_sFirstname} ${mutation.m_sName}`} no={mutation.m_sSocialSecurityNumber}/>
                                            } else {
                                                if (mutation.m_sEmployerName === this.state.firmaFilter) {
                                                    return <Mutation key={'mutation' + idx} changeable={mutation.m_oProcessingState === 0} type={mutation.m_sEventType} owner={`${mutation.m_sFirstname} ${mutation.m_sName}`} no={mutation.m_sSocialSecurityNumber}/>
                                                } else {
                                                    return null
                                                }
                                            }
                                        })
                                    }
                                    <Button className="d-inline-block text-uppercase font-14 text-center text-sm-left mt-0 mb-5" onClick={() => {
                                            console.log("password forgot clicked")
                                        }} to={'/versicherte/mutationen'} buttonType={EButtonType.BasicLink}>
                                        Alle Mutationen
                                        <div className="iconWrapper small ml-2">
                                            <IconArrow/>
                                        </div>
                                    </Button>
                                </div>
                            </div>

                            <div className="col-lg-4 col-12">
                                <div className="columnContent">
                                    <div className="columnHeader">
                                        <h1>Offene Rechnungen</h1>
                                        <p>Ihre offenen Rechnungen</p>
                                        <Button
                                            className="d-flex text-uppercase font-14 text-left mt-auto"
                                            onClick={() => {
                                                    console.log("password forgot clicked")
                                                }}
                                            href="http://www.orimi.com/pdf-test.pdf"
                                            buttonType={EButtonType.FullWidthInverted}>
                                            Übersichts-PDF herunterladen
                                            <div className="iconWrapper small ml-auto w-auto" style={{
                                                    height: '20px',
                                                    marginTop: '-5px'
                                                }}>
                                                <IconPdfDownload className="" style={{
                                                        height: '25px'
                                                    }}/>
                                            </div>
                                        </Button>
                                    </div>
                                    {
                                        this.props.invoices.map((invoice, idx) => {
                                            if (this.state.firmaFilter === "") {
                                                return <Invoice key={'invoice' + idx} state={invoice.state} amount={invoice.amount} number={invoice.number} date={invoice.date}/>
                                            } else {
                                                if (invoice.m_sEmployerName === this.state.firmaFilter) {
                                                    return <Invoice key={'invoice' + idx} state={invoice.state} amount={invoice.amount} number={invoice.number} date={invoice.date}/>
                                                } else {
                                                    return null
                                                }
                                            }
                                        })
                                    }
                                    <Button className="d-inline-block text-uppercase font-14 text-sm-left text-center mt-0 mb-5" onClick={() => {
                                            console.log("password forgot clicked")
                                        }} to={'/rechnungen'} buttonType={EButtonType.BasicLink}>
                                        Alle Rechnungen
                                        <div className="iconWrapper small ml-2">
                                            <IconArrow/>
                                        </div>
                                    </Button>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>);
    }
}

function mapStateToProps(state : RootState, ownProps : any) {
    return {
        ...ownProps,
        mutations: state.app.mutations,
        all: state.app.all,
        invoices: state.app.invoices
    }
}

function mapDispatchToProps(dispatch : Dispatch < actions.ACTION >) {
    return {
        // onIncrement: () => dispatch(actions.incrementEnthusiasm()),
        // onDecrement: () => dispatch(actions.decrementEnthusiasm()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cockpit);
