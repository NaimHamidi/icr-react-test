import React from 'react';
import {connect} from 'react-redux';
import {RootState} from '../../reducers/index';
import {Dispatch} from 'redux';
import * as actions from '../../actions/';
// import { Button, EButtonType } from '../../components/Button/index';
import Header from '../../components/Header';
import ExpandableRow from '../../components/ExpandableRow';
import Drawer from '../../components/Drawer';
// import { Button, EButtonType } from '../../components/Button/index';
import {Input} from '../../components/Input/index';
import {EValidationTestType} from '../../utils/validationConfigs';

import classNames from 'classnames';
// import { WithContext as ReactTags } from 'react-tag-input';
// import { EValidationTestType } from '../../utils/validationConfigs';
// import { Input } from '../../components/Input/index';
// import { ReactComponent as IconArrow } from '../../assets/icons/arrow-right.svg';
import {ReactComponent as IconFilter} from '../../assets/icons/filter.svg';

import {ReactComponent as IconPdfDownload} from '../../assets/icons/pdfdownload.svg';
// import {ReactComponent as IconCheckmarkWhite} from '../../assets/icons/checkmark-bold-white.svg';
import './Invoices.scss';

const statusOptions = [
    {
        id: '1',
        dropdownLabel: 'überfällig'
    }, {
        id: '2',
        dropdownLabel: 'offen'
    }
];

const firmaOptions = [
    {
        id: '154',
        name: "154",
        dropdownLabel: 'Firma 144'
    }, {
        id: '155',
        name: "155",
        dropdownLabel: 'Firma 145'
    }
];

interface State {
    statusFilter: string;
    firmaFilter: string;
    filtersOpened: boolean;
    filteredInvoices: any;
}

interface Props {
    mutations: Array<any>;
    invoices: Array<any>;
    all: any;
}

class Invoices extends React.Component<Props, State> {
    constructor(props : any) {
        super(props);
        document.body.classList.add("invoicesPage");
        this.state = {
            statusFilter: "",
            firmaFilter: "",
            filtersOpened: window.innerWidth >= 1400,
            filteredInvoices: props.all.Data
        }
    }

    drawer: any = null;

    viewPortChanged = (e) => {
        console.log("viewPortChanged", e);
        setTimeout(() => {
            if (e.target.innerWidth >= 1400 && (this.state.filtersOpened === false)) {
                this.setState({filtersOpened: true});
            }
        }, 500);
    }

    componentDidMount() {
        console.log('main app did mount', this.props);
        document.body.classList.remove('backdrop');
        window.addEventListener("resize", this.viewPortChanged);
    }

    componentWillUnmount() {
        document.body.classList.remove("invoicesPage");
        window.removeEventListener("resize", this.viewPortChanged);
    }

    handleContentContainerClick = e => {
        if (this.drawer.contains(e.target)) {
            return;
        } else {
            window.innerWidth < 1400 && this.setState({filtersOpened: false});
        }

    }

    parseDate = (str) => {
        if (str) {
            var options = {
                day: 'numeric',
                month: '2-digit',
                year: 'numeric',
                // hour: '2-digit',
                // minute: '2-digit'
            };
            return new Date(Date.parse(str)).toLocaleDateString('de-DE', options);
        } else
            return ''
    };

    renderState(untilDate, delayed) {
        return <div className="state">
            <div className={classNames('pill', {
                    open: untilDate === null || !delayed,
                    'delayed': untilDate && delayed
                })}>
                {
                    delayed
                        ? "überfällig"
                        : "offen"
                }
            </div>
        </div>
    }

    renderMoreContent(entry) {
        return (
            <div className="moreContainer container-fluid flex-wrap">
            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Lohn
                        </div>

                        <div className="value">
                            CHF {entry.m_nGrossSalary}
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column">
                            Bonus
                        </div>

                        <div className="value">
                            CHF {entry.m_nGrossSalary}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column">
                            Kassennummber
                        </div>

                        <div className="value">
                            {entry.m_sSocialSecurityNumber}
                        </div>

                    </div>
                </div>

                <div className="column col4">
                    <div className="flex-column">

                        <div className="title column">
                            Versicherungsnummer
                        </div>

                        <div className="value">
                            {entry.m_sSocialSecurityNumber}
                        </div>

                    </div>
                </div>
            </div>

            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Lohn gültig von
                        </div>

                        <div className="value">
                            {this.parseDate(entry.m_dPayrollValidFrom)}
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column">
                            Arbeitspensum
                        </div>

                        <div className="value">
                            {entry.m_nActivityRate}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column">
                            Arbeitgeber
                        </div>

                        <div className="value">
                            {entry.m_sEmployerName}
                        </div>

                    </div>
                </div>

                <div className="column col4 d-none d-md-inline-flex">
                    <div className="flex-column">

                        <div className="title column"></div>

                        <div className="value"></div>

                    </div>
                </div>
            </div>

            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Lohn gültig bis
                        </div>

                        <div className="value">
                            {this.parseDate(entry.m_dPayrollValidUntil)}
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column">
                            Eintrittsdatum
                        </div>

                        <div className="value">
                            {this.parseDate(entry.m_dBirthDate)}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column">
                            Firma
                        </div>

                        <div className="value">
                            {entry.m_sFirstname}
                        </div>

                    </div>
                </div>

                <div className="column col4">
                    <div className="flex-column">

                        <div className="title column"></div>

                        <div className="value"></div>

                    </div>
                </div>
            </div>
            <div className="row">
                <div className="column col1">
                    <div className="flex-column">

                        <div className="title column">
                            Adresse
                        </div>

                        <div className="value">
                            {entry.m_sFirstname}
                            15
                        </div>

                    </div>
                </div>

                <div className="column col2">
                    <div className="flex-column">

                        <div className="title column ">
                            Zivilstand
                        </div>

                        <div className="value showChildOnHover">
                            {entry.m_sName}
                        </div>

                    </div>
                </div>

                <div className="column col3">
                    <div className="flex-column">

                        <div className="title column"></div>

                        <div className="value"></div>

                    </div>
                </div>

                <div className="column col4">
                    <div className="flex-column">

                        <div className="title column"></div>

                        <div className="value"></div>

                    </div>
                </div>
            </div>

        </div>
        )
    }

    filterInvoices(name, value) {
        console.log("filterInvoices", name, value);

        const doTheFiltering = () => {
            const filtered = this.props.all.Data;
            filtered.forEach((invoice) => {
                invoice['filteredByFirma'] = true;
                invoice['filteredByStatus'] = true;

                if (this.state.firmaFilter !== "") {
                    invoice['filteredByFirma'] = (invoice.m_sEmployerName === this.state.firmaFilter)
                };

                if (this.state.statusFilter !== "") {
                    if (this.state.statusFilter === "überfällig") {
                        console.log("uberfallig return", invoice.m_dPayrollValidUntil ? Date.parse(invoice.m_dPayrollValidUntil) < Date.now() : false);
                        invoice['filteredByStatus'] = invoice.m_dPayrollValidUntil ? Date.parse(invoice.m_dPayrollValidUntil) < Date.now() : false;
                    } else {
                        console.log("else", (invoice.m_dPayrollValidUntil === null || Date.parse(invoice.m_dPayrollValidUntil) > Date.now()))
                        invoice['filteredByStatus'] = (invoice.m_dPayrollValidUntil === null || Date.parse(invoice.m_dPayrollValidUntil) > Date.now());
                    }
                }
            });

            this.setState({filteredInvoices: filtered});
        }

        // onChange={(name, value) => this.setState({ firma: value }) }
        if (name === "firmaFilter") {
            this.setState({firmaFilter: value}, () => doTheFiltering());
        } else {
            this.setState({statusFilter: value}, () => doTheFiltering());
        }
    }

    determineRowClasses(invoice, existingClasses) {
        let classes = [
            invoice.filteredByFirma === false && 'd-none',
            invoice.filteredByStatus === false && 'd-none'
        ];
        // console.log("classes: ", invoice.filteredByFirma, invoice.filteredByStatus, invoice.m_sEmployerName ,classes);
        // console.log("determineRowClasses: ", version, classes.some(cl => cl === 'd-none') ? existingClasses + ' d-none' : existingClasses);
        return classes.some(cl => cl === 'd-none') ? existingClasses + ' d-none' : existingClasses;
    }


    render() {
        return (<div className="p-relative">
            <Header title="Rechnungen">
            <div className="d-block d-xl-none iconWrapper pointer" style={{
                    position: 'absolute',
                    right: '20px',
                    top: '30px'
                }} onClick={(e) => {
                    e.stopPropagation();
                    this.setState({
                        filtersOpened: !this.state.filtersOpened
                    })
                }}>
                <IconFilter/>
            </div>
            </Header>
            <div className="contentContainer invoices d-flex" onClick={(e) => this.handleContentContainerClick(e)}>
                <div className="container-fluid w-100" style={{}}>
                    <div className="customContainer">
                        <div className="">
                            <div className="headerRow">

                                <div className={classNames('column title', 'col1')}>
                                    Rechngsnummer
                                </div>
                                <div className={classNames('column title', 'col2')}>
                                    Rechnungsdatum
                                </div>
                                <div className={classNames('column title', 'col3')}>
                                    Fälligkeitsdatum
                                </div>
                                <div className={classNames('column title', 'col4')}>
                                    Betrag
                                </div>
                                <div className={classNames('column title', 'col5')}>
                                    Status
                                </div>
                                <div className={classNames('column title', 'col6 icon')}></div>
                                <div className={classNames('column title', 'col7 icon')}>
                                    <div className="iconWrapper pointer d-none d-xl-block filterIcon" style={{
                                            position: 'absolute',
                                            right: '32px'
                                        }} onClick={(e) => {
                                            e.stopPropagation();
                                            this.setState({
                                                filtersOpened: !this.state.filtersOpened
                                            })
                                        }}>
                                        <IconFilter/>
                                    </div>
                                </div>

                            </div>
                            {
                                this.state.filteredInvoices.map((entry, idx) => {
                                    return <ExpandableRow
                                            className={this.determineRowClasses(entry, "")}
                                            key={'invoiceRow' + idx}
                                            col1={'CH' + entry.m_sInsEmployeeId}
                                            col2={this.parseDate(entry.m_dPayrollValidFrom)}
                                            col3={this.parseDate(entry.m_dPayrollValidUntil)}
                                            col4={entry.m_nGrossSalary}
                                            col5={this.renderState(entry.m_dPayrollValidUntil, Date.parse(entry.m_dPayrollValidUntil) < Date.now())}
                                            col6={<div className = "iconWrapper large" style = {{}} >
                                                <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer">
                                                <IconPdfDownload/>
                                        </a>
                                    </div>} moreContent={this.renderMoreContent(entry)}/>
                                })
                            }
                        </div>

                    </div>
                </div>
                <div className=""ref={node => this.drawer = node}>

                    <Drawer opened={this.state.filtersOpened}>
                    {/*<Button
              className="d-inline-block font-14 text-right font-500 mt-2 mb-5"
              onClick={() => { this.setState({filtersOpened: false}) }}
              to={null}
              buttonType={EButtonType.BasicLink}
            >
              Einklappen
            </Button>
            <p className="colorGrey mt-5 mb-1">Auswahl filtern</p>*/
                    }
                    <div className="paddingContainer">
                        <Input
                            id="inputId" name="statusFilter"
                            inputWrapperClass="w-100"
                            placeholder="Rechnungsstatus"
                            value={this.state.statusFilter}
                            onChange={(name, value) => this.filterInvoices(name, value)}
                            onDropdownOpen={(opened) => {
                                    console.log("onDropdownOpen", opened)
                                }}
                            selectOptions={statusOptions}
                            validationMessageConfig={null/* this.validationMessageConfig() */}
                            type='text'
                            clearable
                            validationTests={[EValidationTestType.hasValue]}
                            inputWrapperStyle={{}}></Input>
                        <Input
                            id="inputId"
                            name="firmaFilter"
                            inputWrapperClass="w-100"
                            placeholder="Firma wahlen"
                            value={this.state.firmaFilter}
                            onChange={(name, value) => this.filterInvoices(name, value)}
                            onDropdownOpen={(opened) => {
                                    console.log("onDropdownOpen", opened)
                                }}
                            selectOptions={firmaOptions} validationMessageConfig={null/* this.validationMessageConfig() */}
                            type='text'
                            clearable
                            validationTests={[EValidationTestType.hasValue]}
                            inputWrapperStyle={{}}></Input>
                    </div>

                    <div className="breaker"></div>

                    <div className="paddingContainer">
                        <p className="colorGrey mb-5" style={{
                                marginTop: '42px'
                            }}>Aktionen</p>
                        <div className="d-flex">
                            <a href="http://www.orimi.com/pdf-test.pdf" target="_blank" rel="noopener noreferrer" className="w-100 d-flex">
                                <p className="colorMain font-14">Liste als PDF herunterladen</p>
                                <div className="iconWrapper small ml-auto w-auto" style={{
                                        height: '20px',
                                        marginTop: '-5px'
                                    }}>
                                    <IconPdfDownload className="" style={{
                                            height: '25px'
                                        }}/>
                                </div>
                            </a>
                        </div>
                    </div>
                </Drawer>
                </div>
            </div>
        </div>);
    }
}

function mapStateToProps(state : RootState, ownProps : any) {
    return {
        ...ownProps,
        mutations: state.app.mutations,
        all: state.app.all,
        invoices: state.app.invoices
    }
}

function mapDispatchToProps(dispatch : Dispatch < actions.ACTION >) {
    return {
        // onIncrement: () => dispatch(actions.incrementEnthusiasm()),
        // onDecrement: () => dispatch(actions.decrementEnthusiasm()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Invoices);
